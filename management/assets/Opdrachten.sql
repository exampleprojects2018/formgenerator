-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2018 at 08:29 AM
-- Server version: 5.5.58-log
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `Opdrachten`
--

CREATE TABLE IF NOT EXISTS `Opdrachten` (
  `OpdID` int(11) NOT NULL,
  `OpdOmschrijving` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Opdrachten`
--

INSERT INTO `Opdrachten` (`OpdID`, `OpdOmschrijving`, `_token`) VALUES
(1, 'Veen Media', '$2y$10$RWKi.ByUJxjLBeJgFXsY8uhFGjPSLzSOmndFxKRHjLzuRTcdYpStS'),
(15, 'Opzij', '$2y$10$RWKi.ByUJxjLBeJgFXsY8uhFGjPSLzSOmndFxKRHjLzuRTcdYpStS'),
(301, 'ZPRESS Young', '$2y$10$RWKi.ByUJxjLBeJgFXsY8uhFGjPSLzSOmndFxKRHjLzuRTcdYpStS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Opdrachten`
--
ALTER TABLE `Opdrachten`
  ADD PRIMARY KEY (`OpdID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
