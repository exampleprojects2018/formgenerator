<?php



class ViewController {

		public function __construct() {

				$this->smarty = new Smarty();
				$this->smarty->setTemplateDir(VIEW);

		}

		public function initializeData($view, $param1 = null, $param2 = null){

				$aData = null;

				$pos = strrpos($view, "-");
				$viewMethod = str_replace("-","",$view);
				$ctrlName = ucfirst(explode("-", $view)[0]) . "Controller";
				$subOld = substr($viewMethod, $pos, strlen($viewMethod));
				$subNew = ucfirst(substr($viewMethod, $pos, strlen($viewMethod)));

				$viewMethod = str_replace($subOld,$subNew,$viewMethod);

				if(method_exists($this,$viewMethod)){

						$this->Ctrl = new $ctrlName();
						$aData = $this->$viewMethod($param1, $param2);
				}

				return $aData;
		}



		public function showView($view, $param1 = null, $param2 = null) {


				$aData = $this->initializeData($view, $param1, $param2);

				# HIDE / SHOW TOP MENU AND LOGOUT BTN 
				if($view == "login"){
						define('HIDE_NAV', 'hide');
				}
				else{
						define('HIDE_NAV', '');				
				}

				include 'views/partials/header.php';

				$this->smarty->assign('aData', $aData);
				
				try {
					$this->smarty->display($view .'.tpl');
				}
				catch(Exception $e){
					$this->smarty->display('page404.tpl');
					//die($e);
				}
			
				include 'views/partials/footer.php';

				# REMOVE ALL SESSION AFTER LOAD THE PAGE "LOGIN" 
				if($view == "login"){
						session_destroy();
				}

		}

		public function formNew($OpdID){
				return $this->Ctrl->getFormPlatformsCategories($OpdID);
		}

		public function formEdit($OpdID){
				return $this->Ctrl->getFormPlatformsCategories($OpdID);
		}

		public function formList($OpdID) {
				return $this->Ctrl->getOpdForms($OpdID);
		}

		public function formExample($OpdID, $FormID){
				return $this->Ctrl->getPreview($OpdID, $FormID);
		}


		public function categoryList($OpdID){

				return $this->Ctrl->getCategories($OpdID);
		}





}