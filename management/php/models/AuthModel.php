<?php

class AuthModel {

		public function __construct() {
			
				$this->Db = new Database();
		}


		public function authenticate($OpdrachtOms, $token, $OpdrachtID){

				$sql = "SELECT * FROM " .TBL_OPDRACHTEN. " WHERE OpdID = :OpdID  AND OpdOmschrijving = '" . $OpdrachtOms ."'";
				
				$aBind = [
							':OpdID' => $OpdrachtID,
							':OpdOmschrijving' => $OpdrachtOms
						];

				$res = $this->Db->fetchRow($sql, $aBind);

				$auth = password_verify($token, $res['_token']);

				if($auth){
						return $res["OpdID"];
				}
				else{
						return false;
				}
		}
}
