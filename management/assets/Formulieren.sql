-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2018 at 08:27 PM
-- Server version: 5.5.58-log
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `Formulieren`
--

CREATE TABLE IF NOT EXISTS `Formulieren` (
  `FrmID` int(11) NOT NULL,
  `FrmOpdID` int(11) NOT NULL,
  `FrmPlatform` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FrmCatID` int(11) NOT NULL,
  `FrmNaam` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Formulieren`
--

INSERT INTO `Formulieren` (`FrmID`, `FrmOpdID`, `FrmPlatform`, `FrmCatID`, `FrmNaam`, `Timestamp`) VALUES
(72, 1, '2', 4, 'Opzeggen', '2018-01-24 15:40:20'),
(75, 1, '3', 3, 'Test', '2018-02-02 14:58:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Formulieren`
--
ALTER TABLE `Formulieren`
  ADD PRIMARY KEY (`FrmID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Formulieren`
--
ALTER TABLE `Formulieren`
  MODIFY `FrmID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
