
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="<?php echo ASSETS; ?>/css/style.css">
  
  <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css">


  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Pragati+Narrow" rel="stylesheet">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<!-- Ion Icon -->
	<link rel="stylesheet" type="text/css" media="screen" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <script src="<?php echo ASSETS; ?>/js/vendor.js"></script>
  <script src="<?php echo ASSETS; ?>/js/layout.js"></script>
  <script src="<?php echo ASSETS; ?>/js/form-builder.min.js"></script>
  <script src="<?php echo ASSETS; ?>/js/form-render.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.js"></script>
  <script src="<?php echo ASSETS; ?>/js/action.js"></script>
  	<!-- jQuery Validator -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

  <title></title>
</head>

<body>
	<?php include 'modal.php'; ?>
  <div class="content" >

  	<?php 

  			if($Form) {
  				include 'views/main.content.php';
  			}
  			else{
  				
  				include 'views/notfound.php';
  			}
  	?>

  </div>
</body>

</html>
