<?php 

		//error_reporting(E_ALL); ini_set('display_errors', 'On'); 

		/* =========================================================
		 * SYS INIT
		 * ====================================================== */
		session_start();

		mb_internal_encoding("UTF-8");
		mb_http_output("UTF-8");
		ini_set('default_charset', 'UTF-8');
		header('Content-Type: text/html; charset=UTF-8');

		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');


		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_TIME, 'nl_NL');
		
		ini_set('memory_limit','252M'); 
		ini_set('max_execution_time', '60'); // Extend execution time to a minute

		define ('HOST' , $_SERVER['HTTP_HOST']);
		define ('REQ_METHOD' , $_SERVER['REQUEST_METHOD']);
		define ('DOC_ROOT', 			__DIR__);

		if(HOST == "url_develop"){
				define ('WEB_ROOT' ,'http://' .HOST. '/forms.nl/');
				define ('BASE_PATH' , '/iframe');
		}
		else{

				define ('WEB_ROOT' ,'https://' .HOST. '/forms.nl/');
				define ('BASE_PATH' , '/iframe');
		}

		define ('VIEW' ,DOC_ROOT .'/views');
		define ('ASSETS' ,WEB_ROOT .'iframe/assets');
		set_include_path(DOC_ROOT);

		/* =========================================================
		 * MySql CREDENTIALS
		 * ====================================================== */
		define('DB_HOST','localhost');
		define('DB_USERNAME','username');
		define('DB_PASSWORD','password');
		define('DB_DATABASE','database');
		define('DB_CHARSET','utf8');

		/* =========================================================
		 * INCLUDE SERVICES
		 * ====================================================== */

		include 'services/PHP-Router/src/Route.php';
		include 'services/PHP-Router/src/RouteCollection.php';
		include 'services/PHP-Router/src/Router.php';

		include 'services/Database.class.php';


		/* =========================================================
		 * 		INCLUDE CLASSES
		 * ====================================================== */
		include ('services/functions.inc.php');

		# include the router
		require 'services/appRouter.php';
