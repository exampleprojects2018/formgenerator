$( document ).ready(function() {

 setTimeout(function(){
						
		$('#contentWrap').fadeIn('slow');
		//$('#contentWrap').show('slow');

		$(document).on("click", ".goPrevious", function(){
		    window.history.back();
		});

		$("#modalCancel").on('click', function(){

			$(".feedback").fadeOut('fast');
		})

		$("div.feedback > ").on('click', function(){

			$(".feedback").fadeOut('fast');
		});

		switch(view) {


		    /* FORM VIEWS */


				 case "welkom":

				 		addTitle('WELKOM BIJ PERFECT', 'center');

		        break;

		    case "form-list":

		    		addTitle('FORMULIEREN', 'left');
		        break;

		    case "form-new":

		       	initializeNewFormGenView();
		    		addTitle('NIEUW FORMULIER', 'left');
		        break;

		    case "form-edit":


		       	initializeEditFormGenView();
		    		addTitle('FORMULIER AANPASSEN', 'left');
		        break;

		    case "voorbeeld":

		    		showVoorbeeld();
		    		addTitle('VOORBEELD', 'left');
		        break;
		    /* FORM VIEWS END*/

		    /* PLATFORM VIEWS */
		    case "platform-new":

		    		addTitle('NIEUW PLATFORM', 'left');
		        break;

		     case "platform-list":

		    		addTitle('PLATFORM', 'left');
		        break;

		    /* PLATFORM VIEWS END*/

		    /* PLATFORM VIEWS */
		    case "category-new":

		    		addTitle('NIEUW CATEGORY', 'left');
		        break;

		     case "category-list":

		    		addTitle('CATEGORIEEN', 'left');
		        break;

		    /* PLATFORM VIEWS END*/

		    /* STYLESHEETS VIEWS */
		    case "stylesheets":

    				initializeColorPicker();
						initializeFontPicker();
						initializePlatforms();

		    		addTitle('STIJL AANPASSEN', 'left');
		        break;

		    /* STYLESHEETS VIEWS END */

		}

		}, 20);


		function addTitle(title, align){
			
				$( "#Title" ).load(baseRoot+ "/views/partials/title.php");

				setTimeout(function(){
						$(".title").text(title);
						$(".title").css('text-align', align);
	
				}, 20);
		}
});

function showFeedbackModal(title, message, success, confirm, callback){

		$(".feedback").removeClass('success-feedback');
		$(".feedback").removeClass('error-feedback');

		if(success){
			$(".feedback").addClass('success-feedback');
		}
		else{
			$(".feedback").addClass('error-feedback');
		}
		$(".feedback .feedback_title").empty();
		$(".feedback .message").empty();
		$(".feedback .feedback_title").append('<p>' + title + '</p>');
	  $(".feedback .message").append('<p>'+ message + '</p>');
	  $(".feedback").fadeIn('fast');

	  if(confirm){

	  		confirm_elem = '<button class="btn btn-default" id="modalConfirm" action="'+callback +'">Ja</button>';
	  		$(".feedback .message").append(confirm_elem);
	  		$(".feedback .message").append('<button class="btn btn-default" id="modalCancel">Annuleren</button>');
	  }
	  else{

		  setTimeout(function(){   
		  	$(".feedback").fadeOut('fast');
			}, 4000);	
	  }

}

$(document).on('click', "#modalConfirm", function(){
		var function_to_call = $(this).attr('action');
		window[function_to_call]();
});


