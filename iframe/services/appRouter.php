<?php

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;

$collection = new RouteCollection();


$collection->attachRoute(new Route('/insertForm', array(
    
    '_controller' => 'FormController::insertForm',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/getStyle/:OpdID/:PlatformID', array(
    
    '_controller' => 'FormController::getStyle',
    'methods' => 'GET'

)));


$collection->attachRoute(new Route('/:id/:formID', array(
    
    '_controller' => 'FormController::showForm',
    'methods' => 'GET'

)));

$router = new Router($collection);

//$router->setBasePath(BASE_PATH);

$route = $router->matchCurrentRequest();

