
<div class="row">


		<div class="col-lg-5 col-md-5 col-sm-5">
			<div class="col-lg-8 col-md-8 col-sm-8">
				
				<h3>Platform</h3>
				<hr>
				<select id="stylePlatform" >
						<option disabled selected>-- Kiezen --</option>
						
				</select>
			</div>
		</div>
		
		<div class="col-lg-7 col-md-7 col-sm-7 form-group " id="StyleContent">

				<h3>Stijlen</h3>
				<hr>
				<div  class="row">

							<div class="col-sm-4 col-md-3 col-lg-3">
								<label for="StijlBg" class="col-form-label">Form kleur</label>
							</div>
							<div id="StijlBgDiv" class="input-group colorpicker-component colorpicker-element col-sm-6 col-md-4 col-lg-4">
							    <input type="text" value="#fff" class="form-control" id="StijlBg" >
							    <span class="input-group-addon"><i id="icon-bg-color" ></i></span>
							</div>

				</div>

				<div  class="row" id="ColorPickFont">

							<div class="col-sm-4 col-md-3 col-lg-3">
								<label for="StijlFontColor" class="col-form-label">Lettertype kleur</label>
							</div>

							<div id="StijlFontColorDiv" class="input-group colorpicker-component colorpicker-element col-sm-6 col-md-4 col-lg-4">
							    <input type="text" value="#000" class="form-control" id="StijlFontColor">
							    <span class="input-group-addon"><i id="icon-font-color"></i></span>
							</div>
				</div>

				<div class="row" id="FontFamily">
						<div>

							<div class="col-sm-4 col-md-3 col-lg-3">
									<label for="	StijlFont" class="col-form-label">Lettertype</label>
							</div>
							<div class="col-sm-6 col-md-4 col-lg-4">

								<div id="fontSelect" class="fontSelect" id="StijlFont" >
									<div class="arrow-down"></div>
								</div>
									<input type="hidden" value="" id="SelectedFont">
								<!-- <select class=" form-control input-sm" id="StijlFont">

										<option id="kiezenFontFamily" selected value="">-- Kiezen --</option>
										<option id="TimesNewRoman" value="Times New Roman">Times New Roman</option>
										<option id="Arial" value="Arial">Arial</option>
										<option id="Tahoma" value="Tahoma">Tahoma</option>

								</select> -->
							</div>
						</div>
				</div>

				<!-- <div class="row">
							
							<div class="col-sm-4 col-md-3 col-lg-3">
								<label for="StijlFontSize" class="col-form-label">Lettertype grootte</label>
							</div>
							<div class="col-sm-6 col-md-4 col-lg-4">
								<input type="number" name="StijlFontSize" class=" form-control input-sm" id="StijlFontSize">
							</div>

				</div> -->

				<div class="row">
						<div id="FontWeight">		

							<div class="col-sm-4 col-md-3 col-lg-3">
									<label for="StijlFontSize" class="col-form-label">Lettertype grootte</label>
							</div>

							<div class="col-sm-6 col-md-4 col-lg-4">
									
										<select class=" form-control input-sm" id="StijlFontSize">
												<option id="KiezenFontSize" selected value="">-- Kiezen --</option>
												<option id="size_9" value="9">9</option>
												<option id="size_11" value="11">11 px</option>
												<option id="size_13" value="13">13 px</option>
												<option id="size_15" value="15">15 px</option>
												<option id="size_17" value="17">17 px</option>
												<option id="size_19" value="19">19 px</option>
												<option id="size_21" value="21">21 px</option>
												<option id="size_23" value="23">23 px</option>
												<option id="size_25" value="25">25 px</option>
												<option id="size_27" value="27">27 px</option>
										</select>
							
							</div>
						</div>
				</div>

				<div class="row">
						<div id="FontWeight">		

							<div class="col-sm-4 col-md-3 col-lg-3">
									<label for="StijlFontWeight" class="col-form-label">Lettertype dikte</label>
							</div>

							<div class="col-sm-6 col-md-4 col-lg-4">
									
										<select class=" form-control input-sm" id="StijlFontWeight">
												<option id="kiezenFontweight" selected value="">-- Kiezen --</option>
												<option id="normal" value="normal">normal</option>
												<option id="bold" value="bold">dik</option>
										</select>
							
							</div>
						</div>
				</div>

				
		</div>

</div>

<div class="row" id="StyleSave" >

	<div class="col-lg-10">
		<p id="errorText" style="display:none;"></p>
	</div>
	<div class="col-lg-2">
			<button class="btn btn-success" id="Opslaan" onclick="saveStyle()">Opslaan</button>
	</div>
</div>

<div class="row" id="HrBlock">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<hr class="hrStyle">
	</div>
</div>


