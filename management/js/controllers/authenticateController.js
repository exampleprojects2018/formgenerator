$(document).ready(function(){
	111
		// check if the user authenticated on every page load
		checkAuthenticated();

		// auth by clicking on the button
		$(document).on('click', '#login', function(event){
		    authenticate();
		});

		// auth by clicking pushing enter
		$(document).on('keyup',"#token" , function(e){
				
				if(e.which == 13){
						authenticate();
				}
		});

		// keep the session alive on every movement
		$(document).on('click', 'body', function(){

				var rootPath = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1)

				if(rootPath != 'login'){ 
						// keep alive
						$.get( baseRoot + "/keepAlive", function( res ) {});
				}
		});

		$("#BtnLogout").on("click", function(){

						$.get( baseRoot + "/action/logout", function( res ) {});
						localStorage['auth'] = false;
						window.location.replace(baseRoot+ "/view/login");
		})


		function checkAuthenticated(){

				var rootPath = window.location.pathname.substr(window.location.pathname.lastIndexOf('/') + 1)

				$.get( baseRoot + "/action/getAuthenticated", function( res ) {

						var auth = jQuery.parseJSON(res);

						if(rootPath != 'login'){

								if(!auth.result){
										$.get( baseRoot + "/action/logout", function( res ) { });
										window.location.replace(baseRoot+ "/view/login");
								}
								else if(auth.result == "loggedOut"){ 
										window.location.replace(baseRoot+ "/view/login");
								}
								else if(auth.result == "SessionExpired"){ 
										$.get( baseRoot + "/action/logout", function( res ) { });
										window.location.replace(baseRoot+ "/view/login");
									
								}

						}

				});
		}




		function authenticate() {

				var OpdrachtOms = $("#Opdracht option:selected" ).text();
				var OpdrachtID = $("#Opdracht option:selected" ).val();
				var token = $("#token").val();

				dataValues = { OpdrachtOms : OpdrachtOms , token : token , OpdrachtID : OpdrachtID ,};

				$.ajax({

				    url: baseRoot +"/action/authenticate",
				    type: "POST",
				    data: dataValues,
				    traditional: true,
								    
				}).done(function(result) {

						var res = jQuery.parseJSON(result);

						if(res.result){

								localStorage['auth'] = true;
								window.location.replace(baseRoot+ "/view/welkom");
						}
						else{
							$(".error").show();
							$(".error").text("Onjuist wachtwoord!");
						}

				});
		}
	
});
