<?php



class FormModel {

		public function __construct() {
			
				$this->Db = new Database();
		}


		public function getCategories($OpdID) {

				$sql = "SELECT * FROM ". TBL_CATEGORIEN ." WHERE CatOpdID = " . $OpdID;
				return $this->Db->fetchArray($sql);
		}

		public function getPlatform($OpdID) {

				$sql = "SELECT * FROM " . TBL_UITGAVEN . " WHERE OpdID = " . $OpdID;

				$result = $this->Db->fetchArray($sql);

				if(count($result) > 0 ){
						return $result;
				}
				else{
						return false;
				}
		}

		public function getForms($FormID) {

				$sql = "SELECT * FROM ". TBL_FORMULIEREN ." LEFT JOIN ".TBL_FORM_ELEMENTEN." ON ". TBL_FORMULIEREN .".FrmID = ".TBL_FORM_ELEMENTEN.".ElmFrmID WHERE FrmID = " . $FormID ." ORDER BY ".TBL_FORM_ELEMENTEN.".ElmVolgorde ASC";

				$result = $this->Db->fetchArray($sql);

				if(count($result) > 0) {
						return $result;
				}
				else{
						return false;
				}

		}

		public function getPreview($OpdID, $FormID) {

				$sql = 
							"SELECT * FROM ". TBL_FORMULIEREN .
							" LEFT JOIN ". TBL_FORM_ELEMENTEN .
							" ON ". TBL_FORMULIEREN .".FrmID = ". TBL_FORM_ELEMENTEN .".ElmFrmID ". 
							" WHERE FrmOpdID = " . $OpdID ." AND FrmID = " . $FormID . 
							" ORDER BY ". TBL_FORM_ELEMENTEN .".ElmVolgorde ASC";

				$result = $this->Db->fetchArray($sql);
				if( count($result) > 0 ){
						return $result;
				}
				else{
					return false;
				}
		} 

		public function getElements() {

				$sql = "SELECT * FROM ". TBL_FORMULIEREN ." WHERE ElmFrmID = 1";
				return $this->Db->fetchArray($sql);
		}

		public function updateOrder($data) {

				$aData = explode(',', $data['volgorde']);

				$sql = "UPDATE ". TBL_FORMULIEREN . 
							" SET " . $data['idfield'] . 
							"Volgorde = '". $aData[0]."'".
							" WHERE " . $data['idfield'] . "ID = " . $aData[1];
				
				$this->Db->execute($sql);
	            	
				
		}

		public function updateStyle($aData, $StijlID) {

				$sValues = $this->Db->prepareDataUpdate($aData);

				$sql = "UPDATE ". TBL_STIJLEN. 
								" SET " . $sValues . 
							" WHERE StijlID = " . $StijlID;
				
				$this->Db->execute($sql);
	            	
				
		}

		public function updateFormulieren($aData, $FormulierenID){

				$sValues = $this->Db->prepareDataUpdate($aData);


				$sql = "UPDATE ". TBL_FORMULIEREN . 
							" SET " . $sValues . 
							" WHERE FrmID = " . $FormulierenID;
				
				$this->Db->execute($sql);

		}
		
		public function insert($aData, $sTable) {
			
				$prepData = $this->Db->prepareDataInsert($aData);

				$sql = 	"
									INSERT INTO 
								" . $sTable . " 
										(". $prepData["sFields"] .") 
							 VALUES 
							 			(" . $prepData["sValues"] .")";	
				
				return $this->Db->execute($sql);

		}

		public function getMAxID() {

				$sql = "SELECT max(`ElmFrmID`) as MaxID FROM ".TBL_FORM_ELEMENTEN;
				return $this->Db->fetchRow($sql);
		}

		public function checkFormdb($CatID, $FormNaam) {

				$sql = "SELECT * FROM ". TBL_FORMULIEREN ." WHERE FrmCatID  = " . $CatID . " AND FrmNaam = '" . $FormNaam ."'";
				return $this->Db->fetchRow($sql);
		}

		public function deleteFormElements($ID) {

				$sql = "DELETE FROM ". TBL_FORM_ELEMENTEN ." WHERE ElmFrmID = " . $ID ;
				$this->Db->execute($sql);
		}

		public function deleteForm($ID) {

				$sql = "DELETE FROM ". TBL_FORMULIEREN ." WHERE FrmID = " . $ID ;
				$this->Db->execute($sql);
		}

		public function getOpdForms($OpdID) {

				$sql = "SELECT * FROM " . TBL_FORMULIEREN .
 								" JOIN " . TBL_UITGAVEN ." ON ". TBL_FORMULIEREN .".FrmPlatform = ". TBL_UITGAVEN .".UitCode ".
								" JOIN ". TBL_CATEGORIEN ." ON ". TBL_CATEGORIEN .".CatID = ". TBL_FORMULIEREN .".FrmCatID ".
								"WHERE 	FrmOpdID  = " . $OpdID ." ".
								"GROUP BY FrmID";

				$res = $this->Db->fetchArray($sql);

				if(count($res) > 0 ){
					return $res;
				}
				else{
					return false;
				}

		}

		public function getStyle($OpdID, $PlatformID) {

				$sql = "SELECT * FROM ". TBL_STIJLEN ." WHERE 	StijlOpdID  = " . $OpdID ." AND StijlPlatformID = " . $PlatformID ;
				$result = $this->Db->fetchRow($sql);

				if (count($result) > 0) {
						return $result;
				}
				else{

						return false;
				}
		}

		
}
