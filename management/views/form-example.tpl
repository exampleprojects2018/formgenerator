<div data="{$aData.OpdID}" id="OpdID" ></div>
<div data="{$aData.FormID}" id="FormID" ></div>

<h3>{$aData.Preview[0].FrmNaam}</h3>
<!--  {count($aData.Preview)|var_dump}  -->
  	<div id="Form" >		

			{assign var="tempSize" value=""}
			{assign var="tempPosition" value=""} 

  	<!--  LOOP START -->

 	{foreach from=$aData.Preview  key=index item=elem} 


	<!-- BUTTON -->
	{if $elem['ElmType'] == 'button' }

		{assign var="sLabel" value=""}
		{assign var="sType" value="button"}
		{assign var="sClass" value="btn btn-default"}
		{if $elem['ElmGrootte'] == "klein"}

			{assign var="sClassSize" value=" col-lg-6 col-md-6 col-sm-6"}
		{else}
			{assign var="sClassSize" value=" col-lg-12 col-md-12 col-sm-12"}
		{/if}

	  {assign var="sValue" value="{$elem['ElmLabel']}"}
		{assign var="sRequired" value="{$elem['ElmVerplicht']}"} <!-- Has to be boolean -->
		
		{assign var="sName" value="Button"}
		{assign var="sID" value="ButtonSubmit"}
		{assign var="sPosition" value="links"}
		{assign var="sId" value="Button"}


	{else}

		{assign var="sLabel" value="{$elem['ElmLabel']}"}
		{assign var="sType" value="{$elem['ElmType']}"}
		{assign var="sClass" value="form-control"}
		{if $elem['ElmGrootte'] == "klein"}

			{assign var="sClassSize" value=" col-lg-6 col-md-6 col-sm-6"}
		{else}
			{assign var="sClassSize" value=" col-lg-12 col-md-12 col-sm-12"}
		{/if}

	  {assign var="sValue" value=""}
		{assign var="sRequired" value="{$elem['ElmVerplicht']}"} <!-- Has to be boolean -->
		
		{assign var="sName" value="{$elem['ElmNaam']}"}
		{assign var="sID" value="{$elem['ElmNaam']}"}
		{assign var="sPosition" value="{$elem['ElmPositie']}"}
		{assign var="sId" value=""}
	{/if}

  			<!-- the row should open if the element should not be on the right -->
  			{if $sPosition == "links"} <div class="row"> {/if}


		  			<div class="form-group {$sClassSize}" id="{$sId}">

							  <label for="{$sName}">
							  		{$sLabel}			
							  </label>

							  {if $sType != "textarea"}

							  <input 
								  	type="{$sType}" 
								  	class="{$sClass}" 
								  	id="{$sID}" 
								  	name="{$sName}"
								  	value="{$sValue}"
								  	required = "{$sRequired}"

								  	disabled
								">
								{else}
								<textarea rows="4" cols="50"

									class="{$sClass}" 
									id="{$sID}" 
									name="{$sName}"
									required = "{$sRequired}"

									disabled
								>
									
								</textarea>
								{/if}

						</div>

					<!--
					# the row should be closed if the next row has to be on the left
					# otherwise we leave the row open to be able to add new element
					# or close if the last element 
					-->
					{if 
							array_key_exists($index + 1, $aData.Preview)
							&& 
							$aData.Preview[$index + 1]['ElmPositie'] != "rechts" 	
							||
							count($aData.Preview) == $index + 1
					}
		
					</div>
					{/if}

	{/foreach}
	<!--  LOOP END -->
		</div>

		<div class="row" id="actionBtnBlock">
				<div class="col-lg-12 col-md-12 col-sm-12">

						<div class="row" id="HrBlock">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<hr class="hrStyle">
							</div>
						</div>
						
						<div class="col-lg-2 col-md-2  col-sm-3 col-xs-6">
							<a href="{$smarty.const.WEB_ROOT}/view/form-new/{$aData.OpdID}">
									<button class="btn btn-success">Nieuw formulier aanmaken</button>
							</a>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
							<a href="{$smarty.const.WEB_ROOT}/view/form-edit/{$aData.OpdID}/{$aData.FormID}">
									<button class="btn btn-warning">Huidig formulier aanpassen</button>
							</a>
						</div>
				</div>
			</div>