<!--  {$aData|var_dump}  -->

<div id="toRemove"></div>
<div id="originalVal"></div>

<div id="Categories" class="col-md-6 col-sm-8 ">
		<table class="table table-striped table-bordered">
<!-- 			{$aData.Categories|@count} -->
			<th>Categorie</th>
			<th class="action"></th>
			<th></th>
			<th class="total action">Total: {$aData.Categories|@count}</th>

				{foreach $aData.Categories as $key => $category}

						{if $key%2 == 0}
						<tr class='tooltip-toogle categoriesRow'>
						{/if}
								<td class="editInputCat" id='{$category.CatID}'><input type="edit" name="" value="{$category.CatNaam}" disabled></td>	
								<td class="actionBtns">
									<i class="ion-edit editCat">&nbsp;&nbsp;&nbsp;</i>
									<i class="ion-android-checkbox-outline hide saveCat">&nbsp;&nbsp;&nbsp;</i>
									<i class="ion-trash-a removeCat"></i>
								</td>
						{if $aData.Categories|@count == $key + 1 && $aData.Categories|@count%2 != 0}
							<td></td>
							<td></td>
						{/if}

						{if $key%2 != 0}
						</tr>
						{/if}

				{/foreach}
		</table>
</div>

<div class="col-md-4 col-md-offset-2 col-sm-2 col-sm-offset-1">
	<table class="table table-striped">
		<tr>
			<th>Categorie toevoegen</th>
		</tr>
		<tr>
			<td>Categorie omschrijving</td>
		</tr>
		<tr>
			<td>
				<input type="text" name="CatNaam" value="" id="CatNaam">
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" class="btn btn-default" name="Toevoegen" value="Toevoegen" id="addCat">
			</td>
		</tr>
	</table>
</div>
