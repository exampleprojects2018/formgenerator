
		// http://formbuilder.readthedocs.io/en/latest/formBuilder/demos/basic/


		// SET THE CURRENT OpdID FROM THE URL

		function initializeNewFormGenView(){

						var fbOptions = {
										  	
					    	subtypes: {
					      	text: ['datetime-local']
					    	},
					    
						    disabledActionButtons: ['data'],
					    	// LANGUAGE
						    i18n: {
						       locale: 'nl-NL',

						    },

					      disableFields: 
					      							[
					      								'autocomplete', 
					      								'header', 
					      								'file', 
					      								'hidden', 
					      								'paragraph', 
					      								'number',
					      								'date',
					      								'checkbox-group',
					      								'radio-group',
					      								'select'
					      							],

					      disabledAttrs: 
					      							[	
					      								'value',
					      								'description',
					      								'max',
					      								'maxlength',
					      								'min',
					      								'rows',
					      								'access', 
					      								'step',
					      								'style', 
					      								'inline', 
					      								'toggle', 
					      								'className', 
					      								'subtype', 
					      								'other', 
					      								'multiple'
					      							],

					      typeUserAttrs: {

							      		text: {
									      		name: {
										      			options : {

											      			'Voorletters' : 'Voorletters',
										      				'Achternaam' : 'Achternaam',
										      				'Postcode' : 'Postcode',
										      				'Huisnummer' : 'Huisnummer',
										      				'Email' : 'Email',
										      				'GSM' : 'GSM',
										      				'Postcode_oud' : 'Postcode_oud',
										      				'Huisnummer_oud' : 'Huisnummer_oud',
										      				'Ingangsdatum' : 'Ingangsdatum',
										      				'IBAN' : 'IBAN',
										      				'Factuurnummer' : 'Factuurnummer',
										      				'Relatinummer' : 'Relatinummer'
									      			}
									      		},

									      		size: {

									      				label: 'Grootte',
										      			options : {

											      			'klein' : 'klein',
										      				'groot' : 'groot',
										      				
									      			}
									      		},

									      		position: {
									      				label: 'Positie',
										      			options : {

											      			'links' : 'links',
										      				'rechts' : 'rechts',
										      				
									      			}
									      		},

									          subtype : {
									          	options: {
									              'text': 'text',
									              'email': 'email',
									            },
									          }
						      			},

						      			textarea : {
									      				
									      				name: {
									      					options : {
									      						'Bericht' :'Bericht'
									      					}
									      				},

									      				size: {

											      				label: 'Grootte',
												      			options : {

													      			'klein' : 'klein',
												      				'groot' : 'groot',
												      				
											      			}
											      		},

											      		position: {
											      				label: 'Positie',
												      			options : {

													      			'links' : 'links',
												      				'rechts' : 'rechts',
												      				
											      			}
											      		},
						      			},

						      			date : {
									      				name: {
									      					options : {
									      						'OpzeggingPer' :'Bericht'
									      					}
									      				}
						      			},

						      			button : {
									      				name: {
									      					options : {
									      						'Submit' :'Submit'
									      					}
									      				}
						      			},

						      			select : {
						      				name: {
									      					options : {
									      						'Option 1' :'Option 1',
									      						'Option 2' :'Option 2',
									      					}
									      				}
						      			},
						      			
						      			'checkbox-group' : {

						      				name: {
									      					options : {
									      						'Option 1' :'Option 1',
									      						'Option 2' :'Option 2',
									      					}
									      				}
						      			},

						      			'radio-group' : {

						      				name: {
									      					options : {
									      						'Option 1' :'Option 1',
									      						'Option 2' :'Option 2',
									      					}
									      				}
						      			}

					      },
					    	//  trigger the save function on submit
					    	onSave: function(e, formData) {

					    			var saveData = formBuilder.actions.getData('json', true);
					    			SaveForm(saveData);
					 
						    },
						  };

				 		var formBuilder = $('.build-wrap').formBuilder(fbOptions);

		}

		function initializeEditFormGenView(){

				OpdID = param1;
				FormID = param2;

				$.get( baseRoot +"/action/getFormElements/" +FormID, function(res) {

							  var res = jQuery.parseJSON(res);

								// SELECTED CATEGORY
								$("#cat_" + res.result[0].FrmCatID).attr("selected", "selected");
								// FormNaam
								$("#FormNaam").val(res.result[0].FrmNaam);
								// UitGave
								$("#UitCode_" + res.result[0].FrmPlatform).attr("selected", "selected");

								var defaultFields = [];

								$.each(res.result, function( index, value ) {
										
										if(value.ElmType == "radio"){
												value.ElmType = "radio-group";
										}

										var fields = {};

										if(value.ElmType == 'email'){
												fields.subtype = "email";
												value.ElmType = "text";	
										}

								  	fields.label =  value.ElmLabel;
								  	fields.placeholder =  value.ElmPlaceholder;
								  	fields.name =  value.ElmNaam;
								  	fields.type =  value.ElmType;
								  	fields.required =  getBool(value.ElmVerplicht);
								  	fields.size =  value.ElmGrootte;
								  	fields.position =  value.ElmPositie;

								  	if(value.ElmOptie){
								  		options = value.ElmOptie.split( '|');

										  	fields.values = [];
										  	$.each(options, function( index, option ) {

										  		optArray = option.split( '=');

								  				value = {label: optArray[0], value: optArray[1]};

												  fields.values.push(value);
								  			});
								  			
								  	}
								  			

							  		defaultFields.push(fields);
								});

							  var fbOptions =   {
							  	
							  	defaultFields : defaultFields,
							    subtypes: {
							      text: ['datetime-local']
							    },
							    
							     disabledActionButtons: ['data'],
							    // LANGUAGE
							    i18n: {
							       locale: 'nl-NL',

							      },

							      disableFields: 
							      							[
								      								'autocomplete', 
								      								'header', 
								      								'file', 
								      								'hidden', 
								      								'paragraph', 
								      								'number',
								      								'date',
								      								'checkbox-group',
								      								'radio-group',
								      								'select'
								      						],

							      disabledAttrs: 
							      							[	
							      								'value',
							      								'description',
							      								'max',
							      								'maxlength',
							      								'min',
							      								'rows',
							      								'access', 
							      								'step',
							      								'style', 
							      								'inline', 
							      								'toggle', 
							      								'className', 
							      								'subtype', 
							      								'other', 
							      								'multiple'
							      							],

							      typeUserAttrs: {

														      	text: {
														      		name: {
														      			options : {
														      				'Voorletters' : 'Voorletters',
														      				'Achternaam' : 'Achternaam',
														      				'Postcode' : 'Postcode',
														      				'Huisnummer' : 'Huisnummer',
														      				'Email' : 'Email',
														      				'GSM' : 'GSM',
														      				'Postcode_oud' : 'Postcode_oud',
														      				'Huisnummer_oud' : 'Huisnummer_oud',
														      				'Ingangsdatum' : 'Ingangsdatum',
														      				'IBAN' : 'IBAN',
														      				'Factuurnummer' : 'Factuurnummer',
														      				'Relatinummer' : 'Relatinummer'

														      			}
														      		},

														      		size: {

														      				label: 'Grootte',
															      			options : {

																      			'klein' : 'klein',
															      				'groot' : 'groot',
															      				
														      			}
														      		},

														      		position: {
														      				label: 'Positie',
															      			options : {

																      			'links' : 'links',
															      				'rechts' : 'rechts',
															      				
														      			}
														      		},

														          subtype : {
														          	options: {
														              'text': 'text',
														              'email': 'email',
														            },
														          },

												      			},

												      			textarea : {

												      				name: {
												      					options : {
												      						'Bericht' :'Bericht'
												      					}
												      				},

												      				size: {

														      				label: 'Grootte',
															      			options : {

																      			'klein' : 'klein',
															      				'groot' : 'groot',
															      				
														      			}
														      		},

														      		position: {
														      				label: 'Positie',
															      			options : {

																      			'links' : 'links',
															      				'rechts' : 'rechts',
															      				
														      			}
														      		},
												      			},

												      			button : {
															      				name: {
															      					options : {
															      						'Submit' :'Submit'
															      					}
															      				}
												      			}

							      },

							      
							      // trigger the save function on submit
							    	onSave: function(e, formData) {

							    			var saveData = formBuilder.actions.getData('json', true);
							    			SaveForm(saveData);
							 
								    },
							  };
				 				var formBuilder = $('.build-wrap').formBuilder(fbOptions);
							});

		}

		function SaveForm(formData) {

						OpdID = param1;

						if(param2 == undefined){
								FormID = "";
						}
						else{
								FormID = param2;
						}


						$("#form-error ul").empty();
						var obj = jQuery.parseJSON(formData);

						var checkDouble = checkDuplicateInObject("name",obj);

						FormNaam = $("#FormNaam").val();

						console.log($("#Uitgave option:selected").prop("disabled"))


						if ($("#Uitgave option:selected").prop("disabled")) {
								UitCode = null;
						}else{
								UitCode = $("#Uitgave option:selected").attr("id").split("_")[1];	
						}

						if ($("#Category option:selected").prop("disabled")) {
							CatID = null;
						}
						else{
							CatID = $("#Category option:selected").attr("id").split("_")[1];
						}

						// SET THE POSITION FOR BUTTON AND TEXTAREA TO "links" BY DEFAULT
						$.each(obj, function( index, value ) {

							  if(value.type == "textarea" || value.type == "button" ){
							  		obj[index].position = "links";
							  }

						});

						// SET THE DATA TO STORE
						var data = {obj, CatID:CatID, FormNaam:FormNaam, OpdID: OpdID, UitCode :UitCode, FormID:FormID};

						if (data.obj.length > 0 &&  CatID != null && FormNaam != "" && checkDouble && UitCode != null) {


								$("#form-error").remove();

								$.ajax({
								    url: baseRoot +"/action/insertForm",
								    type: "POST",
								    data: data
								    
								}).done(function(result) {

										var res = jQuery.parseJSON(result);

										if (res) {

											// TRIGGER MODAL
											document.getElementById('modalBtn').click();
											// ADD THE CORRECT LINK TO THE BUTTON ON THE MODAL
											$(".modal-footer a").attr("href" , baseRoot + "/view/form-example/" + OpdID + "/" + res.FormID );
											$("#modalBtn button").text('Naar voorbeeld');
										}
				  					
								});
						}
						else{

								var aMessage = [];

								if (UitCode == null) {
										aMessage.push({"msg" : "Platform moet gekozen worden", "id" : "Uitgave"});
										$("#Uitgave").addClass('has-error');
								}


								if ($("#stage1").find("div.pull-left").hasClass('empty')) {
										aMessage.push({"msg" : "Input moet worden toegevoegd", "id" : "FormGen"});
								}

								if (FormNaam == "") {
										aMessage.push({"msg" : "Formuliernaam is verplicht", "id" : "FormNaam"});
										 $("#FormNaam").addClass('has-error');
								}

								if (CatID == null) {
										aMessage.push({"msg" : "Categorie moet gekozen worden", "id" : "Category"});
										$("#Category").addClass('has-error');
								}

								$.each(aMessage, function( index, error ) {
			
									  $("#form-error ul").append("<li class='"+ error.id +"'>" + error.msg + "</li>");
									  $("#form-error").slideDown("fast");
								});
						}
		}

		function checkDuplicateInObject(propertyName, inputArray) {

					  testObject = {};
						var duplicateElements = [];
					  
					  inputArray.map(function(item) {

						    var itemPropertyName = item[propertyName];  

						    if (itemPropertyName in testObject) {

						      if(duplicateElements.indexOf(itemPropertyName) === -1){
						      		duplicateElements.push(itemPropertyName);	
						      }
						    }
						    else {
						      testObject[itemPropertyName] = item;
						      delete item.duplicate;
						    }
					  });

					  if(duplicateElements.length > 0 ){

								$.each(duplicateElements, function( index, value ) {
					  		
					  				$("#form-error ul").append("<li class='' id='error_"+value+"'>" + value +" kunnen niet worden gedupliceerd</li>");
								});
								
					  		$("#form-error").slideDown("fast");
					  	
					  		return false;
					  }
					  else{

					  		return true;
					  }
		}

		function validate(elem){

				error = $(elem).hasClass('has-error');
				if(error){

						inpVal = $(elem).val();


						if (inpVal != "" || inpVal != null) {

								$(elem).removeClass('has-error');
						}	
				}

				errorId = $(elem).attr('id');

				$("."+errorId).remove();

				li = $("#form-error").find('li');
				if(li.length == 0){
					dismiss();
				}

		}



		function goToEditForm(OpdID, FrmID){

				window.location = viewPath + "form-edit/" + OpdID +"/" + FrmID;
		}

		function getBool(val){ 
		    var num = +val;
		    return !isNaN(num) ? !!num : !!String(val).toLowerCase().replace(!!0,'');
		}

		function dismiss() {

				$("#form-error").hide();
		}


		function showVoorbeeld(){

				$.get( baseRoot +"/action/voorbeeld/" + OpdID +"/"  + FormID, function(res) {

						 var result = jQuery.parseJSON(res);


					$.each(result.data, function( index, elem ) {

							if(elem['ElmType'] == 'button'){

				  				sLabel = "";
				  				sType = 'button';
				  				sClass = "btn btn-default";
				  				sClassSize = elem['ElmGrootte'] == "klein" ? " col-lg-6 col-md-6 col-sm-6" : " col-lg-12 col-md-12 col-sm-12";
				  				sName = 'Button';
				  				sValue = elem['ElmLabel'];
				  				sID = "ButtonSubmit";
				  				sRequired = getBool(elem['ElmVerplicht']);
				  				sPosition = 'links';
				  				sId = "Button";

			  			}

			  			// OTHER FIELDS
			  			else{

			  					sLabel =  elem['ElmLabel'];
			  					sType = elem['ElmType'];
			  					sClass = "form-control";
			  					sClassSize = elem['ElmGrootte'] == "klein" ? " col-lg-6 col-md-6 col-sm-6" : " col-lg-12 col-md-12 col-sm-12";
			  					sName = elem['ElmNaam'];
			  					sValue = "";
			  					sID = elem['ElmNaam'];
			  					sRequired = getBool(elem['ElmVerplicht']);
			  					sPosition = elem['ElmPositie'];
			  					sId = "";


			  			}

			  			if(sPosition == "links"){
			
			  				element = '<div class="row">';
			  				element += "<div class='form-group " + sClassSize + "' id='" + sID + "'>";
			  				element += "<label for='" + sName + "'>" + sLabel + "</label>";


			  			}

			  			if(sType != "textarea"){

			  					element += '<input ' +
									  	'type="' + sType + '" ' + 
									  	'class="' + sClass + '" ' +
									  	'id="' + sID + '" ' +
									  	'name="' + sName + '" ' +
									  	'value="' + sValue + '" ' +
									  	'required = "' + sRequired + '" ' +
									  	'disabled >';

			  			}
			  			else{

			  					element += 
					  					'<textarea rows="4" cols="50"'+

												'class="' + sClass + '" ' +
												'id="' + sID + '" ' +
												'name="' + sName + '" ' +
												'required = "' + sRequired + '" ' +

												'disabled >' +
												
											'</textarea>';


			  			}

			  		//	element += "</div>";

			  			nextElem = index + 1;
							// if(result.data.length == nextElem ){
							// 		element += "</div>";
							// }

							if ( result.data[nextElem]['ElmPositie'] != "rechts") {
								console.log(1)
								console.log(result.data[nextElem]['ElmPositie'])
								element += "</div>";
							}

			  			$("#Form").append(element);

			  				
					});

				});

		}




		 	// 	$("#FormGenContent").on('mouseup', function(){
			 //  		setTimeout(function(){ formGenValidate(formBuilder); }, 200);	
				// });


		$(".iFrameLink i").on('click', function(){

				/* get the text to copy */
				copyText = $(this).parent().find('input').val();
			  /* Select the text field */
			  $(this).parent().find('input').select();
			  /* execute copy */
			  document.execCommand("Copy");

			  /* show copied alert */
			  title = "De link is gekopieerd";
			  message = copyText;
			  showFeedbackModal(title, message, true);

		});

		ctrlDown = false;
		$(".iFrameLinkInput").on('keydown', function(event){

			if(event.keyCode == 17){
				ctrlDown = true;
			}
			event.preventDefault()

		}).keyup(function(e) {

		     if (e.keyCode == 67 && ctrlDown){
						document.execCommand("Copy");
		     } 

		      if (e.keyCode == 65 && ctrlDown){

						 $(this).select();
		     } 

		    ctrlDown = false;
		});


		$('.remove').on('click', function(){

				FormID = $(this).parent().parent().attr('id');

				$("#toRemove").attr('FormID', FormID);

			 /* show copied alert */
			  title = "Fromulier verwijderen";
			  message = "Weet je zeker dat je het formulier wilt verwijderen?";
			  /* title / message / success / confirm */
			  showFeedbackModal(title, message, false, true, 'removeForm');

		});

function removeForm(){

	FormID = $("#toRemove").attr('FormID');
	data = {FormID : FormID};

	$.ajax({
	    url: baseRoot +"/action/removeForm",
	    type: "POST",
	    data: data
	    
	}).done(function(result) {

			var res = jQuery.parseJSON(result);

			$("#" + res.FormID).remove();
			subsTotal = $(".total").text() - 1;
			$(".total").text(subsTotal);

	});
}
