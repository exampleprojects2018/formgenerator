</div>
				</div>
		</div>
   <?php include 'views/modals/feedback.php'; ?>
		<!-- CONFIG -->
		<script src="<?php echo WEB_ROOT; ?>/js/conf.js"></script>

		<!-- AUTHENTICATION -->
		<script src="<?php echo WEB_ROOT; ?>/js/controllers/authenticateController.js"></script>




		<!-- FONT SELECTOR -->
		<script src="<?php echo WEB_ROOT; ?>/js/plugins/jquery.fontselector.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/fontselector.css">
		
		<?php if (VIEW_FORM != false) { ?>
				<!-- FORM BUILDER -->
				<script src="<?php echo WEB_ROOT; ?>/js/form-builder/vendor.js"></script> 
				<script src="<?php echo WEB_ROOT; ?>/js/form-builder/form-builder.min.js"></script>
				<script src="<?php echo WEB_ROOT; ?>/js/form-builder/form-render.min.js"></script>
		<?php } ?>



		<!-- CONTROLLERS -->
		<script src="<?php echo WEB_ROOT; ?>/js/controllers/formController.js"></script>
		<script src="<?php echo WEB_ROOT; ?>/js/controllers/platformController.js"></script>
		<script src="<?php echo WEB_ROOT; ?>/js/controllers/categoryController.js"></script>
		<script src="<?php echo WEB_ROOT; ?>/js/controllers/styleController.js"></script> 
<script src="<?php echo WEB_ROOT; ?>/js/controllers/viewController.js"></script> 
</body>

</html>


