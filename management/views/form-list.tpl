<!--  {$aData.Forms|var_dump}  -->

<div id="toRemove"></div>
<div id="Forms">
		<table class="table table-striped">
			<th>Platform</th><th>Categorie</th><th>Formnaam</th><th>Gemaakt op</th><th>iFrame Link <div style="float:right">Total: <span class="total">{$aData.Forms|@count}</span></div></th><th></th>


				{foreach $aData.Forms as $key => $form}

						<tr class='tooltip-toogle' id='{$form.FrmID}' >
								<td onclick='goToEditForm({$form.OpdID}, {$form.FrmID})'>{$form.UitOmschrijving}</td>
								<td onclick='goToEditForm({$form.OpdID}, {$form.FrmID})'>{$form.CatNaam}</td>
								<td onclick='goToEditForm({$form.OpdID}, {$form.FrmID})'>{$form.FrmNaam}</td> 
								<td onclick='goToEditForm({$form.OpdID}, {$form.FrmID})'>{$form.Timestamp}</td>
								<td class="iFrameLink">
									<input spellcheck="false" type="text" class="iFrameLinkInput" value="{$smarty.const.LINK_IFRAME}/{$form.OpdID}/{$form.FrmID}">
									&nbsp;&nbsp;&nbsp;&nbsp;<i class="ion-ios-copy-outline"></i>
								</td>
								<td><i class="ion-trash-a remove"></i></td>
						</tr>

				{/foreach}
		</table>
</div>
