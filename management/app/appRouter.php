<?php

/* ===============================================================================================

    --- new Route('/getAddress/:postcode/:huisnummer') ---
    --- new Route('/:lidmaatschap/:profile/:code') ---
    
==================================================================================================

    We can pass arguments by using ":" before the name of the parameter.
    In our case if we put ":" before the first parameter it always indicates that 
    it needs to return the view with the name of the parameter.

    First parameter can be with or without ":" . 
    In that case that is an action, so it needs to return a function from a controller.

    We can call the ViewController directly  which will return only the desired page without any data.

    If we want to return a view with data, we need to call a controller to make a request.
    After the request we can pass the data to the View.

=================================================================================================== */

use PHPRouter\RouteCollection;
use PHPRouter\Router;
use PHPRouter\Route;


$collection = new RouteCollection();


# === Address checking === // GET #
$collection->attachRoute(new Route('/getAddress/:postcode/:huisnummer', array(
    
    '_controller' => 'AddressController::getAddress',
    'methods' => 'GET'

)));



# === View with / without data === #
$collection->attachRoute(new Route('/view/:view', array(
    
    '_controller' => 'ViewController::showView',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/view/:view/:param1', array(
    
    '_controller' => 'ViewController::showView',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/view/:view/:param1/:param2', array(
    
    '_controller' => 'ViewController::showView',
    'methods' => 'GET'

)));


# ==============    ACTIONS    ========================= #
$collection->attachRoute(new Route('/action/getAuthenticated', array(
    
    '_controller' => 'AuthController::getAuthenticated',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/action/authenticate', array(
    
    '_controller' => 'AuthController::authenticate',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/action/insertForm', array(
    
    '_controller' => 'FormController::insertForm',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/action/getFormElements/:FormID', array(
    
    '_controller' => 'FormController::getFormElements',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/action/getPlatforms/:OpdID', array(
    
    '_controller' => 'StyleController::getPlatforms',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/action/removeForm', array(
    
    '_controller' => 'FormController::removeForm',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/getStyle/:OpdID/:PlatformID', array(
    
    '_controller' => 'StyleController::getStyle',
    'methods' => 'GET'

)));

$collection->attachRoute(new Route('/saveStyle', array(
    
    '_controller' => 'StyleController::saveStyle',
    'methods' => 'POST'

)));

/* CATEGORIES */
$collection->attachRoute(new Route('/action/insertCategory', array(
    
    '_controller' => 'CategoryController::insertCategory',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/action/removeCat', array(
    
    '_controller' => 'CategoryController::removeCategory',
    'methods' => 'POST'

)));

$collection->attachRoute(new Route('/action/updateCategory', array(
    
    '_controller' => 'CategoryController::updateCategory',
    'methods' => 'POST'

)));

$router = new Router($collection);
//$router->setBasePath('/');
$route = $router->matchCurrentRequest();

// var_dump($route);
