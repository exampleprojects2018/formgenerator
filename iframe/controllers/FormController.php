<?php

class FormController {

		public function __construct() {

				$this->Model = new FormModel();

		}

		public function showForm($OpdID,$FormID= null) {

				$Form = $this->Model->getForms($OpdID,$FormID);
				$Category = $this->Model->getCategory($Form[0]['FrmCatID']);
				$Platform = $this->Model->getPlatformSingle($OpdID, $Form[0]['FrmPlatform']);
				$OpdID = $OpdID;
				$FormID = $FormID;
				$Platform = $Platform['UitOmschrijving'];
				$Category = $Category['CatNaam'];

				include "view.php";
		}


		public function insertForm() {

				$this->Model->insert($_POST, "Vragen");
		} 


		public function getStyle($OpdID, $PlatformID) {

				$aStyle = $this->Model->getStyle($OpdID, $PlatformID);
				
				if($aStyle){
						echo json_encode(['success' => true, "result" => $aStyle]);
				}
				else{
						echo json_encode(['success' => false]);
				}

		}	
}