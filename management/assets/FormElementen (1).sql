-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2018 at 08:41 PM
-- Server version: 5.5.58-log
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `FormElementen`
--

CREATE TABLE IF NOT EXISTS `FormElementen` (
  `ElmID` int(11) NOT NULL,
  `ElmFrmID` int(11) NOT NULL,
  `ElmType` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `ElmLabel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ElmNaam` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ElmPlaceholder` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ElmOptie` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ElmGrootte` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `ElmPositie` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `ElmVolgorde` int(11) NOT NULL,
  `ElmVerplicht` tinyint(1) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=926 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FormElementen`
--

INSERT INTO `FormElementen` (`ElmID`, `ElmFrmID`, `ElmType`, `ElmLabel`, `ElmNaam`, `ElmPlaceholder`, `ElmOptie`, `ElmGrootte`, `ElmPositie`, `ElmVolgorde`, `ElmVerplicht`, `Timestamp`) VALUES
(868, 72, 'text', 'Voornaam', 'Voorletters', '', NULL, 'klein', 'links', 1, 1, '2018-01-24 15:42:18'),
(869, 72, 'text', 'Achternaam', 'Achternaam', '', NULL, 'klein', 'rechts', 2, 1, '2018-01-24 15:42:18'),
(870, 72, 'text', 'Email', 'Email', '', NULL, 'klein', 'links', 3, 1, '2018-01-24 15:42:18'),
(871, 72, 'textarea', 'Reden van opzeggen', 'Bericht', '', NULL, 'klein', 'links', 4, 0, '2018-01-24 15:42:18'),
(872, 72, 'button', 'Verzenden', 'Submit', '', NULL, '', 'links', 5, 0, '2018-01-24 15:42:18'),
(881, 76, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:28:04'),
(882, 77, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:28:06'),
(883, 78, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:29:14'),
(884, 79, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:30:16'),
(885, 80, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:31:08'),
(886, 81, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:31:53'),
(887, 82, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:33:53'),
(888, 83, 'button', 'Knop', 'Submit', '', NULL, '', 'links', 1, 0, '2018-02-02 13:40:09'),
(889, 83, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 2, 0, '2018-02-02 13:40:09'),
(890, 83, 'textarea', 'Groot tekst veld', 'Bericht', '', NULL, 'klein', 'links', 3, 0, '2018-02-02 13:40:09'),
(891, 84, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 13:40:47'),
(896, 74, 'text', 'Relatienummer', 'Relatienummer', 'relatienummer', NULL, 'klein', 'links', 1, 0, '2018-02-02 14:41:43'),
(897, 74, 'text', 'Voornaam', 'Voorletters', 'voornaam', NULL, 'klein', 'rechts', 2, 1, '2018-02-02 14:41:43'),
(898, 74, 'email', 'E-mailadres', 'Email', 'email', NULL, 'groot', 'links', 3, 1, '2018-02-02 14:41:43'),
(899, 74, 'button', 'Verzenden', 'Submit', '', NULL, '', 'links', 4, 0, '2018-02-02 14:41:43'),
(923, 75, 'text', 'Tekst veld', 'Voorletters', '', NULL, 'klein', 'links', 1, 0, '2018-02-02 15:46:55'),
(924, 75, 'text', 'Tekst veld', 'Achternaam', '', NULL, 'klein', 'rechts', 2, 0, '2018-02-02 15:46:55'),
(925, 75, 'button', 'Knop', 'Submit', '', NULL, '', 'links', 3, 0, '2018-02-02 15:46:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `FormElementen`
--
ALTER TABLE `FormElementen`
  ADD PRIMARY KEY (`ElmID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `FormElementen`
--
ALTER TABLE `FormElementen`
  MODIFY `ElmID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=926;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
