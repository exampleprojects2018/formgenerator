<?php

class StyleController 
{
	
			function __construct()
			{
				$this->Model = new StyleModel();
			}

			public function getStyle($OpdID, $PlatformID) {

						$aStyle = $this->Model->getStyle($OpdID, $PlatformID);
						
						if($aStyle){
								echo json_encode(['success' => true, "result" => $aStyle]);
						}
						else{
								echo json_encode(['success' => false]);
						}

			}

			public function saveStyle() {

						$aStyle = $this->Model->getStyle($_POST['StijlOpdID'], $_POST['StijlPlatformID']);

						if(!$aStyle){
							
								$StyleID = $this->Model->insert($_POST, TBL_FORM_STIJLEN);
								echo json_encode(['success' => true, 'message' => "Success save"]);
						}
						else{

								$StyleID = $this->Model->updateStyle($_POST, $aStyle["StijlID"]);
								echo json_encode(['success' => true, 'message' => "Success update"]);
						}

			}

			public function getPlatforms($OpdID) {
					
					$Platform = $this->Model->getPlatform($OpdID);

					if(!$Platform){
							
							echo json_encode(['success' => false]);
					}
					else{
							echo json_encode(['success' => true,  'data' => $Platform]);
					}
			}
}