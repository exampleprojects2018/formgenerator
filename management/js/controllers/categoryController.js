/* Toogle Edit / Save Category */

$('.editCat').on('click', function(){

		$(this).parent().prev().find('input').removeAttr('disabled')
		$(this).parent().prev().addClass('activeEditInput');
		$(this).hide();
		$(this).next(".saveCat").removeClass('hide');
		$(this).next(".saveCat").show();

		originalVal = $(this).parent().prev().find('input').val();
		$("#originalVal").attr('data', originalVal);
});

$('.saveCat').on('click', function(){
	
		$(this).parent().parent().find('input').attr('disabled','disabled')
		$(this).parent().prev().removeClass('activeEditInput');
		$(this).hide();
		$(this).prev('.editCat').show();

		CatNaam = $(this).parent().prev().find('input').val();
		originalVal = $("#originalVal").attr('data');

		if(CatNaam == ""){
			$(this).parent().prev().find('input').filter(':first').val(originalVal);
		}
		else{

			CatID = $(this).parent().prev().attr('id');
			updateCategory(CatNaam,CatID, originalVal);
		}
});

$('.removeCat').on('click', function(){
			
		CatID = $(this).parent().prev().attr('id');
		$("#toRemove").attr('CatID', CatID);

	 /* show copied alert */
	  title = "Fromulier verwijderen";
	  message = "Weet je zeker dat je het categorie wilt verwijderen?";
	  /* title / message / success / confirm */
	  showFeedbackModal(title, message, false, true, 'removeCat');

});

$("#addCat").on('click', function(){

		CatNaam = $("#CatNaam").val();

		if(CatNaam != ""){
			insertCategory(CatNaam);
		}
});

function updateCategory(CatNaam,CatID, originalVal){

	data = {CatNaam : CatNaam, CatID : CatID };

	$.ajax({
	    url: baseRoot +"/action/updateCategory",
	    type: "POST",
	    data: data
	    
	}).done(function(result) {

			var res = jQuery.parseJSON(result);
console.log(res)
			if(res.success == false){

				title = "Duplicate";
				message = "Deze categorie bestaat al";
				/* title / message / success / confirm */
				showFeedbackModal(title, message, false, false, '');
				$("#" + CatID + ' input').val(originalVal);
			}
			else{

			}

	});

}

function insertCategory(CatNaam){

	data = {CatNaam : CatNaam};

	$.ajax({
	    url: baseRoot +"/action/insertCategory",
	    type: "POST",
	    data: data
	    
	}).done(function(result) {

			var res = jQuery.parseJSON(result);
			console.log(res)

			if(res.success == false){

				if(res.error){
					
					title = "Duplicate";
					message = "Deze categorie bestaat al";
					/* title / message / success / confirm */
					showFeedbackModal(title, message, false, false, '');
				}
			}
			else{

					location.reload();
			}

	});

}

function removeCat() {

		CatID = $("#toRemove").attr('CatID');
		data = {CatID : CatID};

		$.ajax({
		    url: baseRoot +"/action/removeCat",
		    type: "POST",
		    data: data
		    
		}).done(function(result) {

				var res = jQuery.parseJSON(result);
				location.reload();

		});
}