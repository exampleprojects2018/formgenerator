<?php 

class CategoryController {

		public function __construct() {

				$this->Model = new CategoryModel();
				$this->View = new ViewController();
		}

		public function getCategories($OpdID){

				# GET DATA FOR THE VIEW
				$Categories = $this->Model->getAll($OpdID);

				$dataReturn = 
					[
							"Categories" => $Categories
					];

				return $dataReturn;
		}

		public function removeCategory() {

				$CatID = $_POST['CatID'];
				
				$this->Model->delete($CatID);

				echo json_encode(['success' => true, 'CatID' => $CatID]);
				exit();
		}

		public function insertCategory(){

				$aData['CatNaam'] = $_POST['CatNaam'];
				$aData['CatOpdID'] = $_SESSION['OpdID'];

				$duplicate = $this->checkDuplicate($aData['CatNaam'], $_SESSION['OpdID']);

				if(!$duplicate){
						$CatID = $this->Model->insert($aData);
						echo json_encode(['success' => true, 'CatID' => $CatID]);
						exit();
				}
				else{
						echo json_encode(['success' => false, 'error' => 'duplicate']);
						exit();
				}

		}

		public function updateCategory(){

				$aData['CatNaam'] = $_POST['CatNaam'];
				$aData['CatID'] = $_POST['CatID'];

				$duplicate = $this->checkDuplicate($aData['CatNaam'], $_SESSION['OpdID']);

				if(!$duplicate){
						$this->Model->update($aData);
						echo json_encode(['success' => true, 'CatID' => $aData['CatID']]);
						exit();
				}
				else{
						echo json_encode(['success' => false, 'error' => 'duplicate']);
						exit();
				}

		}

		public function checkDuplicate($CatNaam, $OpdID){

				# GET DATA FOR THE VIEW
				$Category = $this->Model->getOne($OpdID,$CatNaam);

				if($Category){
					return true;
				}
				else{
					return false;
				}
		}
		

}
