jQuery(document).ready(function(){

		baseRoot = "http://url_develop/iframe";
		var Platform = jQuery("#FrmPlatform").attr('data');
		var OpdID = jQuery("#OpdID").attr('data');

		jQuery.get(baseRoot +"/getStyle/" + OpdID + "/" + Platform, function(res) { 

				var res = jQuery.parseJSON(res);

				if(res.success){

						//	console.log(res.result)

							jQuery("body").css({
									"background-color": res.result.StijlBg,
							});

							jQuery("p, label").css({

									"color":  res.result.StijlFontColor ,
									"font-size" : res.result.StijlFontSize + "px",
									"font-family":  res.result.StijlFont
							});

							jQuery("h3").css({
									"color":  res.result.StijlFontColor,
									"font-family":  res.result.StijlFont
							});

				}

		});

		
});