<?php



class CategoryModel {

		public function __construct() {
			
				$this->Db = new Database();
		}


		public function getAll($OpdID) {

				$sql = "SELECT * FROM ".TBL_CATEGORIEN." WHERE CatOpdID = :OpdID  ORDER BY CatNaam";
				$aBind = [
							':OpdID' => $OpdID
						];

				return $this->Db->fetchArray($sql);
		}

		public function getOne($OpdID, $CatNaam){

				$sql = "SELECT * FROM ".TBL_CATEGORIEN." WHERE CatOpdID = :OpdID AND CatNaam = :CatNaam";
				$aBind = [
							':OpdID' => $OpdID,
							':CatNaam' => $CatNaam
						];

				return $this->Db->fetchRow($sql, $aBind);
		}

		public function delete($ID) {

				$sql = "DELETE FROM ". TBL_CATEGORIEN ." WHERE CatID = :CatID " ;
				$aBind = [
							':CatID' => $ID
						];
				$this->Db->execute($sql,$aBind);
		}

		public function insert($aData) {
			
				$prepData = $this->Db->prepareDataInsert($aData);

				$sql = 	"
									INSERT INTO 
								" . TBL_CATEGORIEN . " 
										(". $prepData["sFields"] .") 
							 VALUES 
							 			(" . $prepData["sValues"] .")";	
				
				return $this->Db->execute($sql);

		}

		public function update($aData) {

				$sql = "UPDATE ". TBL_CATEGORIEN . 
							" SET CatNaam = '" . $aData['CatNaam'] . "'" .
						
							" WHERE CatID = " . $aData['CatID'];
				
				return $this->Db->execute($sql);

		}
}
