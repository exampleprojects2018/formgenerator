<nav class="navbar navbar-default" id="navbar">

  <div class="container-fluid ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
     	<span id="navTitle" class="navbar-brand" href="">Menu</span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="navbar-collapse-1">

      <ul class="nav navbar-nav">
	      	<li class="top-nav" >
	      		<a class="dropdown-toggle" data-toggle="dropdown">Formulieren
	      			<span class="caret"></span>
	      		</a>
		        <ul class="dropdown-menu">
		          <li id="nieuwFormulierBtn"><a href="<?php echo WEB_VIEW_ROOT . "/form-new/" .$_SESSION['OpdID']; ?>">Nieuw formulier aanmaken</a></li>
		          <li id="lijstFormulierenBtn"><a href="<?php echo WEB_VIEW_ROOT . "/form-list/" .$_SESSION['OpdID']; ?>">Formulier lijsten</a></li>
		        </ul>
	      	</li>

	        <li class="" id="navPlatforms">
	        	<a href="<?php echo WEB_VIEW_ROOT . "/platform-list/" .$_SESSION['OpdID']; ?>">Overzicht Platforms</a>
	        </li>

	        </li>

	        <li class="" id="navPlatforms">
	        	<a href="<?php echo WEB_VIEW_ROOT . "/category-list/" .$_SESSION['OpdID']; ?>">Categorieën</a>    	
	        </li>

	        <li class="" id="navStyles"><a href="<?php echo WEB_VIEW_ROOT . "/stylesheets/" . $_SESSION['OpdID']; ?>">Stylesheets</a></li>
      </ul>   

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>