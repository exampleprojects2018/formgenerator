
<form id="formGenerator" class ="" >

    <div class="row" id="NewForm" >

  			<div class="col-md-8" id="FormGenBtnBlock">

    				<div class="col-md-4 col-sm-4">
	    					
				    		<label>Platform</label>
							  <select class="form-control" id="Uitgave" onchange="validate(this)">
							  	<option disabled selected>-- Kiezen --</option>
							    {foreach $aData.Platforms as $key => $platform}
									    <option id="UitCode_{$platform.UitCode}">{$platform.UitOmschrijving}</option>
									{/foreach}
						    </select>
    				</div>

    				<div class="col-md-4 col-sm-4">
    					
				    		<label>Categorie</label>
							  <select class="form-control" id="Category" onchange="validate(this)">
							  	<option disabled selected>-- Kiezen --</option>
							  	{foreach $aData.Categories as $key => $category}
									    <option id="cat_{$category.CatID}">{$category.CatNaam}</option>
									{/foreach}
							    	
						    </select>
    				</div>

    				<div class="col-md-4 col-sm-4">
			    		<label>Formuliernaam</label>
						  <input type="text" name="FormNaam" class="form-control" id="FormNaam" onchange="validate(this)">
						</div>
  			</div>

	    	<div id="form-error" class="col-md-3" style="display: none;">

	  				<strong>Formulier opslaan is mislukt!</strong><i class="ion-close" onclick="dismiss()"></i>
	  				<ul>

	  				</ul>
  			</div>
  	</div>

	  <div class="row" id="FormGenContent">

	  		<div class="col-lg-12">
				    <div id="stage1" class="build-wrap col-lg-12"></div>
				    <form class="render-wrap"></form>
				</div>

	  </div>

</form>