
//$('#StijlFontColor').colorpicker();

function initializeColorPicker(){
    $(function () {
      $('#StijlFontColorDiv').colorpicker();
      $('#StijlBgDiv').colorpicker();
    });
}


		function initializePlatforms(){

				OpdID = param1;

				$.get( baseRoot +"/action/getPlatforms/" + OpdID, function(res) {

				  var res = jQuery.parseJSON(res);

				  if(res.success){
						  $.each(res.data, function(index, value){

						  	option = "<option value='" + OpdID + "/" + value.UitCode + "' >" + value.UitOmschrijving + "</option>";
						  	$("#stylePlatform").append(option)
						  })
				  

				  }
				});
		}

		// INTITALIZE FONTPICKER
		function initializeFontPicker(){

				setTimeout(function(){ 

				$('#fontSelect').fontSelector({
		
					'hide_fallbacks' : true,
					'initial' : 'Courier New,Courier New,Courier,monospace',
					'selected' : function(style) { 

							$("#SelectedFont").val(style);
					},
					'opened' : function(style) { 
						//	alert('opened'); 
					},
					'closed' : function(style) { 
							console.log('closed'); 
					},
					'fonts' : [
							'Arial,Arial,Helvetica,sans-serif',
							'Arial Black,Arial Black,Gadget,sans-serif',
							'Comic Sans MS,Comic Sans MS,cursive',
							'Courier New,Courier New,Courier,monospace',
							'Georgia,Georgia,serif',
							'Impact,Charcoal,sans-serif',
							'Lucida Console,Monaco,monospace',
							'Lucida Sans Unicode,Lucida Grande,sans-serif',
							'Palatino Linotype,Book Antiqua,Palatino,serif',
							'Tahoma,Geneva,sans-serif',
							'Times New Roman,Times,serif',
							'Trebuchet MS,Helvetica,sans-serif',
							'Verdana,Geneva,sans-serif',
							'Gill Sans,Geneva,sans-serif'
						]
				});	
								}, 100);
		}

		// INTITALIZE COLORPICKER
	


		$("#stylePlatform").on("change", function() {

				var Platform = $("#stylePlatform").val();
				var OpdID = Platform.split( '/' )[0];
				var Platform = Platform.split( '/' )[1];

				$.get( baseRoot +"/getStyle/" + OpdID + "/" + Platform, function(res) { 

						var selectedFontWeight = $('#FontWeight').find(":selected").attr('id');
						$("#" + selectedFontWeight).attr('selected', false);

						var res = jQuery.parseJSON(res);

						if(res.success){

									var FontFamily = res.result.StijlFont.split(',')[0];
									var parts = FontFamily.split('"');

									if(parts.length > 1){
										FontFamily = parts[1];
									}
									else{
										FontFamily = parts[0];
									}

									// add the value of the color input field
									$("#StijlBg").val(res.result.StijlBg);	
									$("#StijlFontColor").val(res.result.StijlFontColor);
									// add the value of the font size input field		
									$("#StijlFontSize").val(res.result.StijlFontSize);
									// set the style of the font to show			
									$("#fontSelect").css('font-family', res.result.StijlFont);
									// set the font name to show
									$("#fontSelect span").text(FontFamily);
									$("#SelectedFont").val(FontFamily);
									// change the mini-color-box background to the current
									$("#StijlFontColor").next('span').children('i').css('background-color', res.result.StijlFontColor);
									$("#StijlBg").next('span').children('i').css('background-color', res.result.StijlBg);
									// show the current font size
									$("#"+res.result.StijlFontWeight).attr('selected', true);	 

						}
						else{

									$("#StijlBg").val("#fff");	
									$("#StijlFontColor").val("#fff");		  		
									$("#StijlFontSize").val("");	
									$("#kiezenFontWeight").attr('selected', true);	  		
						}

				});
		});

		function saveStyle() {

				var StijlFont = $("#SelectedFont").val();

				var valid = validateStyleForm();
				if(valid){

						var Platform = $("#stylePlatform").val();
						var OpdID = Platform.split( '/' )[0];
						var Platform = Platform.split( '/' )[1];

						var StijlFont = $("#SelectedFont").val();
						var StijlFontSize = $("#StijlFontSize").val();
						var StijlFontWeight = $("#StijlFontWeight").val();
						var StijlFontColor = $("#StijlFontColor").val();
						var StijlBg = $("#StijlBg").val();

						var saveData = {

								StijlPlatformID : Platform,
								StijlOpdID : OpdID,
								StijlFont : StijlFont,
								StijlFontSize : StijlFontSize,
								StijlFontWeight : StijlFontWeight,
								StijlFontColor : StijlFontColor,
								StijlBg : StijlBg
						}

						$.ajax({
						    url: baseRoot +"/saveStyle",
						    type: "POST",
						    data: saveData
						    
						}).done(function(result) {

								var res = jQuery.parseJSON(result);

								if (res) {

									$("#titleModal").text(res.message);
									$("#messageModal").text('Sluiten');
									$("#modalBtn button").text('Sluiten');
									// TRIGGER MODAL
									document.getElementById('modalBtn').click();
								}
		  					
						});
				}

		}

		function validateStyleForm() {

				var Platform = $("#stylePlatform").val();
				var StijlFont = $("#SelectedFont").val();
				var StijlFontSize = $("#StijlFontSize").val();
				var StijlFontWeight = $("#StijlFontWeight").val();
				var StijlFontColor = $("#StijlFontColor").val();
				var StijlBg = $("#StijlBg").val();

				if(
							Platform == null || 
							StijlFont == undefined ||
							StijlFontSize == "" ||
							StijlFontWeight == null ||
							StijlFontColor == "" ||
							StijlBg == "" 
					){

						/* show copied alert */
	  title = "Mislukt";
	  message = "Stijl is niet correct gedefinieerd";
	  showFeedbackModal(title, message, false);

						// $("#errorText").text("Stijl is niet correct gedefinieerd");
						// $("#errorText").show();
						return false;
				}
				else{
					return true;
				}

		}	
