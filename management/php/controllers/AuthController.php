<?php 
error_reporting(E_ALL); ini_set('display_errors', 'On'); 

class AuthController {

		public function __construct() {

				$this->Model = new AuthModel();
		}

		public function authenticate(){


				$auth = $this->Model->authenticate($_POST['OpdrachtOms'], $_POST['token'], $_POST['OpdrachtID']);

				if($auth) {
						$_SESSION['auth'] = true;
						$_SESSION['SessionExpired'] = false;
						$_SESSION['last_action'] = time();
						$_SESSION['OpdID'] = $_POST['OpdrachtID'];
						echo json_encode(['result' => $auth]);
						exit;
				}
				else{
						$_SESSION['auth'] = false;
						echo json_encode(['result' => false]);
						exit;
				}


		}

		public function logout(){

				$_SESSION['auth'] = false;
				session_destroy();
		} 

		public function keepAlive() {

				$_SESSION['last_action'] = time();
				$_SESSION['SessionExpired'] = false;
		}

		public function getAuthenticated() {

				if(count($_SESSION) == 0){
					
					echo json_encode(['result' => "LoggedOut"]);
					exit();
				}

				# after 60 minutes of inactive user session will be destroyd / 3600 
				if (array_key_exists('last_action', $_SESSION) && $_SESSION['last_action'] < time() - 3600  && $_SESSION['auth'] != false) {

				  	$_SESSION['SessionExpired'] = true;
				  	echo json_encode(['result' => "SessionExpired"]);
				  	exit();
				}

				if(array_key_exists('auth', $_SESSION)){
			
						echo json_encode(['result' => $_SESSION['auth']]);
						exit();
				}

				elseif(array_key_exists('last_action', $_SESSION) && $_SESSION['last_action'] ==  "SessionExpired" || count($_SESSION) == 0)
				{

						echo json_encode(['result' => "SessionExpired"]);
						exit();
				}

				
		}

}

