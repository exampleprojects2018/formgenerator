

<div id="loginView">

		<span id="session"></span>
		<div class="row">

				<div class="form-group col-lg-3 col-md-3 col-sm-3">
				  <label for="Opdracht">Opdracht</label>
				  <select class="form-control" id="Opdracht">
						<option value="1">Veen Media</option>
						<option value="15">Opzij</option>
						<option value="301">ZPRESS Young</option>
				  </select>
				</div>


				<div class="form-group col-lg-3 col-md-3 col-sm-3">
				  <label for="token">Wachtwoord:</label>
				  <input type="password" class="form-control" id="token" autocomplete="nope">
				</div>
		</div>

		<div class="row">
				<div class="form-group col-lg-12 col-md-12 col-sm-12">
						<button type="submit" class="btn-logout" id="login">Login&nbsp;&nbsp;<i class="ion-log-in"></i></button>
						<span class="error" style="display:none"></span>

						{if 'SessionExpired'|array_key_exists:$smarty.session && $smarty.session.SessionExpired == true }
							<span class="expired"> Sessie is verlopen ! </span>
						{/if}
					
				</div>
		</div>

</div>