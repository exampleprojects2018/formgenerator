-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2018 at 08:27 PM
-- Server version: 5.5.58-log
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbconnect`
--

-- --------------------------------------------------------

--
-- Table structure for table `FromStijlen`
--

CREATE TABLE IF NOT EXISTS `FromStijlen` (
  `StijlID` int(11) NOT NULL,
  `StijlOpdID` int(11) NOT NULL,
  `StijlPlatformID` int(11) DEFAULT NULL,
  `StijlFont` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StijlFontColor` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `StijlFontSize` int(11) NOT NULL,
  `StijlFontWeight` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `StijlBg` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `FromStijlen`
--

INSERT INTO `FromStijlen` (`StijlID`, `StijlOpdID`, `StijlPlatformID`, `StijlFont`, `StijlFontColor`, `StijlFontSize`, `StijlFontWeight`, `StijlBg`) VALUES
(21, 1, 10, 'Courier New,Courier New,Courier,monospace', '#ba7373', 11, 'bold', '#3a4aa6'),
(23, 1, 1, 'Courier New,Courier New,Courier,monospace', '#d47070', 11, 'bold', '#fff'),
(24, 1, 2, 'Courier New,Courier New,Courier,monospace', '#ccc6c6', 15, 'normal', '#ad9d26'),
(25, 1, 14, 'Courier New,Courier New,Courier,monospace', '#70336d', 21, 'bold', '#704545'),
(26, 1, 3, '"Lucida Console", Monaco, monospace', '#c99393', 23, 'bold', '#d44646');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `FromStijlen`
--
ALTER TABLE `FromStijlen`
  ADD PRIMARY KEY (`StijlID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `FromStijlen`
--
ALTER TABLE `FromStijlen`
  MODIFY `StijlID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
