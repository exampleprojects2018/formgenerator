
baseRoot = "http://url_develop/forms.nl/iframe";
//baseRoot = "/forms/iframe";
	
jQuery("#ButtonSubmit").on('click', function(){

		var checkVal = validate();

		if (checkVal) {
				
				var dataValues = {};

		    // NORMAL INPUT FIELDS
		    jQuery("Form").find('input').each( function(unusedIndex, child) {

		    		if(child.name != "Button"){

		            dataValues[child.name] = child.value;
		    		}

		    });

		    // TEXTAREA
		    jQuery("Form").find('textarea').each( function(unusedIndex, child) {
		            dataValues[child.name] = child.value;
		    });

		    jQuery.ajax({

				    url: baseRoot +"/insertForm",
				    type: "POST",
				    data: dataValues
				    
				}).done(function(result) {

						//var res = jQuery.parseJSON(result);
						
				});
		}
});

function validate() {

		errorCount = 0;

		jQuery("Form").find('input').each( function(Index, elem) {

				res = checkValue(elem)
				if(!res){
						errorCount++;
				}
		});

		if(errorCount > 0 ){
			return false;
		}
		else{
			return true;
		}

}

function checkValue(elem) {

		elemID = jQuery(elem).attr('id');

		if(jQuery(elem).hasClass('verplicht')){

				if(jQuery(elem).attr('type') == "email"){
						Email = checkEmail(elem, elemID);
				}
				
				if(elem.value == ""){
								
						jQuery("#error_" + elemID).text(" verplicht veld");
						return false;

				}
				if(Email == false && jQuery(elem).attr('type') == "email"){
						
						jQuery("#error_" + elemID).text("ongeldig email adres");				
						return false;
				}
				else{

						jQuery("#error_" + elemID).empty();
						return true;
				}
		}
}

function checkEmail(elem, elemID) {

			emailInput = jQuery('input[type=email]');
			email = 	jQuery('input[type=email]').val();
			if(email != ''){
			//	var pattern = new RegExp("^[a-zA-Z0-9.!#jQuery%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*jQuery");
				var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}jQuery/i);
				var valid_email = pattern .test(email);

				return valid_email;
			}

}
