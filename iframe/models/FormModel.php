<?php



class FormModel {

		public function __construct() {
			
				$this->Db = new Database();
		}

		public function getStyle($OpdID, $PlatformID) {

				$sql = "SELECT * FROM FromStijlen WHERE 	StijlOpdID  = :OpdID  AND StijlPlatformID = :StijlPlatformID" ;

				$aBind = [
							':OpdID' => $OpdID,
							':StijlPlatformID' => $PlatformID
						];
				$result = $this->Db->fetchRow($sql, $aBind);

				if (count($result) > 0) {
						return $result;
				}
				else{

						return false;
				}
		}


		public function getCategories($OpdID) {

			$sql = "SELECT * FROM FormCategorien WHERE CatOpdID = :OpdID";

			$aBind = [
							':OpdID' => $OpdID
						];
			return $this->Db->fetchArray($sql, $aBind);
		}

		public function getPlatform($OpdID) {

			$sql = "SELECT * FROM Uitgaven WHERE OpdID = :OpdID";
			$aBind = [
				':OpdID' => $OpdID
			];
			return $this->Db->fetchArray($sql);
		}

		public function getForms($OpdID,$FormID) {

				$sql = "SELECT * FROM Formulieren LEFT JOIN FormElementen ON Formulieren.FrmID = FormElementen.ElmFrmID WHERE FrmOpdID = " . $OpdID ." AND FrmID = " . $FormID . " ORDER BY `FormElementen`.`ElmVolgorde` ASC";

				$result = $this->Db->fetchArray($sql);
				if( count($result) > 0 ){
						return $result;
				}
				else{
					return false;
				}

		}

		public function getElements() {

			$sql = "SELECT * FROM FormElementen WHERE ElmFrmID = 1";
			return $this->Db->fetchArray($sql);
		}

		public function updateOrder($data) {

				$aData = explode(',', $data['volgorde']);

				$sql = "UPDATE ". $data['table'] . 
							" SET " . $data['idfield'] . 
							"Volgorde = '". $aData[0]."'".
							" WHERE " . $data['idfield'] . "ID = " . $aData[1];
				
				$this->Db->execute($sql);		
		}
		
		public function insert($aData, $sTable) {
			
			$prepData = $this->Db->prepareDataInsert($aData);

			$sql = 	"
								INSERT INTO 
							" . $sTable . " 
									(". $prepData["sFields"] .") 
						 VALUES 
						 			(" . $prepData["sValues"] .")";	
			
			return $this->Db->execute($sql);

		}

		public function getMAxID() {

			$sql = "SELECT max(`ElmFrmID`) as MaxID FROM `FormElementen`";
			return $this->Db->fetchRow($sql);
		}

		public function checkFormdb($CatID, $FormNaam) {

			$sql = "SELECT * FROM Formulieren WHERE FrmCatID  = " . $CatID . " AND FrmNaam = '" . $FormNaam ."'";
			return $this->Db->fetchRow($sql);
		}

		public function deleteFormElements($ID) {

			$sql = "DELETE FROM FormElementen WHERE ElmFrmID = " . $ID ;
			$this->Db->execute($sql);
		}

		public function deleteForm($ID) {
			
			$sql = "DELETE FROM Formulieren WHERE FrmID = " . $ID ;
			$this->Db->execute($sql);
		}

		public function getCategory($CatID) {

				$sql = "SELECT CatNaam FROM FormCategorien WHERE CatID = " . $CatID;
				return $this->Db->fetchRow($sql);
		}

		public function getPlatformSingle($OpdID, $UitCode) {

				$sql = "SELECT UitOmschrijving FROM Uitgaven WHERE OpdID = ". $OpdID ." AND UitCode = " . $UitCode;
				return $this->Db->fetchRow($sql);
		}
		
}
