<?php 

	/* =========================================================
	 * SYS INIT
	 * ====================================================== */
	// error_reporting(E_ALL); ini_set('display_errors', 'On'); 
	session_start();
	//session_destroy();

	mb_internal_encoding("UTF-8");
	mb_http_output("UTF-8");
	ini_set('default_charset', 'UTF-8');
	header('Content-Type: text/html; charset=UTF-8');
	date_default_timezone_set('Europe/Amsterdam');
	setlocale(LC_TIME, 'nl_NL');


	$pos = strpos($_SERVER['SCRIPT_NAME'],'/index.php');
	$Web_root = substr($_SERVER['SCRIPT_NAME'], 0, $pos);

	define ('OFFLINE' , true);

	define ('HOST' , $_SERVER['SERVER_NAME']);
	define ('REQ_METHOD' , $_SERVER['REQUEST_METHOD']);
	define ('DOC_ROOT', 			__DIR__);
	define ('WEB_ROOT' , $Web_root);
	define ('VIEW' ,DOC_ROOT .'/views');
	define ('WEB_VIEW_ROOT' , WEB_ROOT.'/view');
	define ('CSS' ,WEB_ROOT .'/css');
	define ('ASSETS' ,WEB_ROOT .'/assets');

	define('VIEW_FORM', strpos( $_SERVER["REQUEST_URI"],'view/form' ));

	$linkIframe = HOST == "url_develop" ? "url_develop/iframe" : "url_production/iframe" ;
	define('LINK_IFRAME', $linkIframe);

	set_include_path(DOC_ROOT);

	$test = strpos($_SERVER["REQUEST_URI"], "sub_folder_name") == 1 ? true : false;

	/* =========================================================
	 * MySql CREDENTIALS
	 * ====================================================== */
	if($test){

		define('DB_HOST','localhost');
		define('DB_USERNAME','username');
		define('DB_PASSWORD','password');
		define('DB_DATABASE','database');
		define('DB_CHARSET','utf8');

		# AS WE USE THE SAME DATABASE FOR TEST ADN PROD
		define("TBL_FORMULIEREN","Forms_Formulieren" );							// TEST TABLE
		define("TBL_FORM_ELEMENTEN","Forms_FormElementen" );	// TEST TABLE
		define("TBL_STIJLEN","Forms_FromStijlen" );												// TEST TABLE
		define("TBL_UITGAVEN","Forms_Uitgaven" );												// TEST TABLE
		define("TBL_CATEGORIEN","Forms_FormCategorien" );					// TEST TABLE
		define("TBL_OPDRACHTEN","Forms_Opdrachten" );							// TEST TABLE
	}
	else{

		define('DB_HOST','localhost');
		define('DB_USERNAME','username');
		define('DB_PASSWORD','password');
		define('DB_DATABASE','database');
		define('DB_CHARSET','utf8');

		# AS WE USE THE SAME DATABASE FOR TEST ADN PROD
		define("TBL_FORMULIEREN","Formulieren" );							// PROD TABLE
		define("TBL_FORM_ELEMENTEN","FormElementen" );	// PROD TABLE
		define("TBL_STIJLEN","FromStijlen" );												// PROD TABLE
		define("TBL_UITGAVEN","Uitgaven" );												// PROD TABLE
		define("TBL_CATEGORIEN","FormCategorien" );					// PROD TABLE
		define("TBL_OPDRACHTEN","Opdrachten" );								// PROD TABLE
	}

		 


		/* =========================================================
		 * INCLUDE SERVICES
		 * ====================================================== */

		include 'services/PHP-Router/src/Route.php';
		include 'services/PHP-Router/src/RouteCollection.php';
		include 'services/PHP-Router/src/Router.php';
		
		//include 'services/Database.php';
		include 'services/Database.class.php';


		/* =========================================================
		 * 		INCLUDE CLASSES
		 * ====================================================== */

		include ('services/functions.inc.php');

		/* =========================================================
		 * 		INCLUDE SMARTY
		 * ====================================================== */
		require('lib/smarty/Smarty.class.php');