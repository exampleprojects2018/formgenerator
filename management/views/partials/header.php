<!DOCTYPE html>
<html>

<head>
			<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/main.css">

			<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/formGenerator.css">
			<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/icon.css">

			<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT; ?>/css/styleGenerator.css">

			<link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.1/jquery.rateyo.min.css">


			<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
			<link href="https://fonts.googleapis.com/css?family=Pragati+Narrow" rel="stylesheet">
			<!-- Ion Icon -->
			<link rel="stylesheet" type="text/css" media="screen" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	    <link href="//cdnjs.cloudflare.com/ajax/libs/octicons/3.5.0/octicons.min.css" rel="stylesheet">
	    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css" rel="stylesheet">

	    <script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
	    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.js"></script>

			<title>Formulieren</title>

</head>

<body >

    <?php include 'views/modal.php'; ?>
 
    <div class="content" id="mainContent" >

	      <div id="header">
	          <div class="Logout <?php echo HIDE_NAV; ?>">
	                  <button class="btn-logout" id="BtnLogout">Logout &nbsp;&nbsp;<i class="ion-log-out"></i></button>
	          </div>
	      
	          <div id="PG-logo" >
	                  <img src="<?php echo ASSETS; ?>/PG-logo.jpg" alt="" height="40px">
	          </div>
	      </div>

	      <div id="mainNav" class="<?php echo HIDE_NAV; ?>">
	          <?php include 'views/partials/header-nav.php'; ?>
	      </div>
	      <br>


	      <div id="contentWrap" style="display:none"  >
	          <div id="Title"></div>
	          <div id="viewContent" >



