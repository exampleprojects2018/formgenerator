<?php

class FormController {

		public function __construct() {

				$this->Model = new FormModel();
				$this->View = new ViewController();
		}

		public function getFormPlatformsCategories($OpdID){

				# GET DATA FOR THE VIEW
				$Categories = $this->Model->getCategories($OpdID);
				$Platforms = $this->Model->getPlatform($OpdID);

				$dataReturn = 
					[
							"Categories" => $Categories,
							"Platforms" => $Platforms
					];

				return $dataReturn;
		}

		public function getCategories($OpdID,$FormID= null) {

				# GET DATA FOR THE VIEW
				$Categories = $this->Model->getCategories($OpdID);
				$Platform = $this->Model->getPlatform($OpdID);

				# SET DATA TO BE ABLE TO CHANGE VIEW BETWEEN ADD or EDIT
				$newForm = true;
				$FormElements = false;

				# IF FormID WAS PROVIDED THE VIEW SHOULD BE EDIT
				if($FormID){
						$FormElements = $this->Model->getForms($FormID);
						$newForm = false;
				}

				$OpdID = $OpdID;
				$FormID = $FormID;

				include "view.php";
		}

		public function getPreview($OpdID,$FormID= null) {

				$FormPreview = $this->Model->getPreview($OpdID,$FormID);

				$OpdID = $OpdID;
				$FormID = $FormID;
				$aData = array('OpdID' => $OpdID, 'FormID' => $FormID , 'Preview' => $FormPreview);

				return $aData;

		}

		public function getFormElements($FormID){

				$FormElements = $this->Model->getForms($FormID);

				if($FormElements){

						echo json_encode(['success' => true , 'result' => $FormElements]);
						exit();
				}
				else{

						echo json_encode(['success' => false]);
						exit();
				}
		
		}

		
		public function insertForm() {


				$OpdID = $_POST['OpdID'];
				# set data for "Formulieren"
				$aFormulieren['FrmCatID'] = $_POST['CatID'];
				$aFormulieren['FrmNaam'] = $_POST['FormNaam'];
				$aFormulieren['FrmOpdID'] = $OpdID;
				$aFormulieren['FrmPlatform'] = $_POST['UitCode'];

				# delete previously created form elements
				if($_POST['FormID'] != "") {

					# delete old form elements
					$this->Model->deleteFormElements($_POST['FormID']);
					$FormID = $_POST['FormID'];
					
					$this->Model->updateFormulieren($aFormulieren, $FormID);
				}
				# INSERT NEW FORM ELEMENTS
				else{

						# insert table "Formulieren"
						$FormID = $this->Model->insert($aFormulieren, TBL_FORMULIEREN);
				}



				foreach ($_POST['obj'] as $index => $value) {

						# set data for "FormElementen"
						$aFormElementen['ElmFrmID'] =  $FormID;
						$aFormElementen['ElmLabel'] =   $value["label"];
						$aFormElementen['ElmPlaceholder'] =   array_key_exists('placeholder', $value) ? $value["placeholder"] : null; 
						$aFormElementen['ElmNaam'] =  $value["name"];
						$aFormElementen['ElmGrootte'] =  array_key_exists('size', $value) ? $value["size"] : "";
						$aFormElementen['ElmPositie'] =   array_key_exists('position', $value) ? $value["position"] : "";
						$aFormElementen['ElmVolgorde'] =  $index + 1;
						$aFormElementen['ElmVerplicht'] = array_key_exists('required', $value) ? 1 : false;

						# checkbox 
						if($value["type"] == "checkbox-group"){
								$aFormElementen['ElmType'] =  "checkbox";

								$options = "";
								foreach ($value["values"] as $option) {
										$options .= $option['label'] ."=" .$option['value'] ."|" ;
								}

								$aFormElementen['ElmOptie'] =  rtrim($options, "|");
						}
						# text can be password / email / text
						elseif($value["type"] == "text"){
								$aFormElementen['ElmType'] = $value["subtype"];
						}
						elseif($value["type"] == "radio-group"){

								$aFormElementen['ElmType'] =  "radio";

								$options = "";
								foreach ($value["values"] as $option) {
										$options .= $option['label'] ."=" .$option['value'] ."|" ;
								}

								$aFormElementen['ElmOptie'] =  rtrim($options, "|");
						}
						# select
						elseif($value["type"] == "select"){
							
								$aFormElementen['ElmType'] =  "select";
								$options = "";
								foreach ($value["values"] as $option) {
										$options .= $option['label'] ."=" .$option['value'] ."|" ;
								}

								$aFormElementen['ElmOptie'] =  rtrim($options, "|");
						}
						else{
								$aFormElementen['ElmType'] = $value['type'];
						}


						# insert table "FormElementen"
						$this->Model->insert($aFormElementen, TBL_FORM_ELEMENTEN);

						$aFormElementen['ElmLabel'] =   $value["label"];
						unset($aFormElementen['ElmOptie']);
				}

				echo json_encode(['success' => true, 'FormID' => $FormID]);
				exit();
		
		}

		public function ViewPlatform($OpdID) {

				$OpdID = $OpdID;
				$PlatformView = true;
				include "view.php";
		}



		public function getOpdForms($OpdID){

			$aForms = $this->Model->getOpdForms($OpdID);

			return ['Forms' => $aForms];

		}

		public function removeForm(){

				$FormID = $_POST['FormID'];
				
				$this->Model->deleteFormElements($FormID);
				$this->Model->deleteForm($FormID);

				echo json_encode(['success' => true, 'FormID' => $FormID]);
				exit();

		}
		
}