<div data="<?php echo $OpdID ?>" id="OpdID" ></div>
<div data="<?php echo isset($FormID) ? $FormID : ""; ?>" id="FormID" ></div>
<div data="<?php echo $Form[0]["FrmPlatform"]; ?>" id="FrmPlatform" ></div>

	  <h3><?php echo $Form[0]["FrmNaam"]; ?></h3>

  	<form id="Form" autocomplete="off">		

		<input type="hidden" value="<?php echo $Category; ?>" name="Onderwerp" id="Onderwerp">
		<input type="hidden" value="<?php echo $Form[0]["FrmNaam"]; ?>" name="Subonderwerp" id="Subonderwerp">
		<input type="hidden" value="<?php echo $OpdID; ?>" name="OpdrachtID" id="OpdrachtID">
		<input type="hidden" value="<?php echo $Platform; ?>" name="Platform" id="Platform">

  	<!--  LOOP START -->
  	<?php foreach ($Form as $index => $elem) { 

  			# BUTTON
  			if($elem['ElmType'] == 'button'){

	  				$sType = $elem['ElmType'];
	  				$sClass = "btn btn-default";
	  				$sName = 'Button';
	  				$sValue = $elem['ElmLabel'];
	  				$sID = "ButtonSubmit";
	  				$sRequired = (bool)$elem['ElmVerplicht'];
	  				$sPosition = $elem['ElmPositie'];
	  				$sId = "Button";

  			}

  			# OTHER FIELDS
  			else{

  					$sRequired = (bool)$elem['ElmVerplicht'];
  					$sLabel =  $elem['ElmLabel'];
  					$sType = $elem['ElmType'];
  					$sClass = "form-control";
  					$sClass .= $sRequired == true ? " verplicht" : "";
  					$sClassSize = $elem['ElmGrootte'] == "klein" ? " col-lg-6 col-md-6 col-sm-6" : " col-lg-12 col-md-12 col-sm-12";
  					$sName = $elem['ElmNaam'];
  					$sValue = "";
  					$sID = $elem['ElmNaam'];
  					$sPosition = $elem['ElmPositie'];
  					$sId = "";

  			}
  	?>
  	<?php 

  			# the row should open if the element should not be on the right
  			if($sPosition == "links") {	?>	<div class="row"> <?php } ?> 


	  			<div class="form-group <?php echo $sClassSize; ?>" id="<?php echo $sId; ?>">

						  
						  <!-- Button handled differently -->
						  <?php if($sType != "button"){ ?>

						  		<!-- If it's not button , we can put the label name  -->
						  		<label for="<?php echo $sName; ?>">
								  		<?php 
								  			echo $sLabel;
								  			if ($sRequired) { 
								  					echo " *";
								  			}
								  		?>	
								  </label>
								  <!-- If it's not button , we can put the error text next to the label -->
						  		<span class="error" id="error_<?php echo $sID; ?>"></span>

						  <?php } ?>
						  <!-- Normal input fields -->
						  <?php if($sType != "textarea"){ ?>

								  <input 
									  	type="<?php echo $sType;?>" 
									  	class="<?php echo $sClass; ?>" 
									  	id="<?php echo $sID; ?>" 
									  	name="<?php echo $sName; ?>"
									  	value="<?php echo $sValue;?>"
									  	required = "<?php echo $sRequired; ?>"
											onchange="checkValue(this)"
											autocomplete='<?php echo $sType;?>'
									>

							<?php }else{ ?>

									<textarea rows="4" cols="50"

											class="<?php echo $sClass; ?>" 
											id="<?php echo $sID; ?>" 
											name="<?php echo $sName; ?>"
											required = "<?php echo $sRequired; ?>"
											onchange="checkValue(this)"
									></textarea>

							<?php } ?>

					</div>

					<?php 
					# the row should be closed if the next row should not be on the right
					# or if the last element 
					if( 
								array_key_exists($index + 1, $Form) && $Form[$index + 1]['ElmPositie'] != "rechts" 	||
								count($Form) == $index + 1
						) 
						{	
							echo "</div>"; 
						}


					?>


  		<?php } ?>
			<!--  LOOP END -->
			</form>