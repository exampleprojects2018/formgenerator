<!DOCTYPE html>
<html>
		<head>
			<title>App Reference</title>

			<!-- BOOTSTRAP -->
		    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>

		    <!-- Latest compiled and minified CSS -->
		    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

		    <!-- Latest compiled and minified JavaScript -->
		    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>

			<style type="text/css">
				body {
					background: #3f7689;
				}

				#content {
					
					margin: 0 auto;
					height: 100%;
					width: 1000px;
					background-color: #e0e1e4;
					padding:20px 50px 50px 50px;
					margin-top: 30px;
					margin-bottom: 30px;
					min-height: 500px;
				}

				h3 {
					text-align: center;
				}

				.header {
					padding-bottom: 15px;
				}

				li {
					cursor: pointer;
				}

			</style>
		</head>

<body>

	<div id="content">
		
			<div class="header">		
				<h3>Reference</h3>
				<hr>
			</div>

			<div class="row">
				<div class="col-lg-6">

					<h4>CopernicaModel.php</h4>
					<br>
					<ul>
					    <li>
					    	<p>setRequirements()</p>
					    </li>
					    <li>
					    	<p>authProfile()</p>
					    	
					    </li>
					    <li>
					    	<p>searchProfile()</p>
					    	
					    </li>
					    <li>
					    	<p>getSubProfile()</p>
					    	
					    </li>
					    <li>
					    	<p>getInterests()</p>
					    	
					    </li>
					    <li>
					    	<p>saveProfile()</p>
					    	
					    </li>
					    <li>
					    	<p>updateProfile()</p>
					    	
					    </li> 
					    <li>
					    	<p>saveSubProfile()</p>
					    	
					    </li>
					    <li>
					    	<p>profileField()</p>
					    	
					    </li>
					    <li>
					    	<p>sendMail()</p>
					    	
					    </li>
					    <li>
					    	<p>sendSms()</p>
					    	
					    </li>
					    <li>
					    	<p>changeInterest()</p>
					    	
					    </li>
					</ul>	
				</div>

				<div class="col-lg-6">


					<h4>MySqlModel.php</h4>
					<br>
					<ul>
					    <li>
					    	<p>retrieve()</p>
					    </li>
					    <li>
					    	<p>insert()</p>
					    	
					    </li>
					    <li>
					    	<p>update()</p>
					    	
					    </li>
					    <li>
					    	<p>delete()</p>
					    	
					    </li>
					   
					</ul>	
				</div>

				</div>
			</div>

</body>
</html>
